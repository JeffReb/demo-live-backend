// env file
require('dotenv').config();
// Imports
var bcrypt = require('bcrypt');
var jwtUtils = require('../utils/jwt.utils');
var cassandra = require('cassandra-driver');
var asyncLib = require('async');
var emailUtils = require('../utils/email.utils');
var createBucket = require('../utils/upload/createBucket');
const { Client } = require('@elastic/elasticsearch');

// env variables cassandra
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;

// env variables elastic
const elasticHost = process.env.ELASTIC_HOST;
const elasticIndex = process.env.ELASTIC_INDEX;
const elascticHostadress = elasticHost + ':9200';



var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);


var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});

const clientElastic = new Client({ node: elascticHostadress });

client.connect(function (err, result) {
    console.log('index: cassandra connected to user');
});

// Constants
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const PASSWORD_REGEX = /^(?=.*\d).{4,12}$/;

// Routes
module.exports = {
    register: function(req, res){
        // TODO: To implement

        // Params
        var email = req.body.email;
        var name = req.body.name;
        var password = req.body.password;
        var id = cassandra.types.TimeUuid.now();
        var added_date = cassandra.types.generateTimestamp();
        var saltRounds = 5;
        var bcryptedPassword =  bcrypt.hashSync(password, saltRounds);

        // test and control timstamp with console.log
        // **************************************
        // var dateA = added_date.toString();
        // var dateB = dateA.substring(13,0);
        // var d = new Date(+dateB);
        // console.log(id);
        // console.log(added_date);
        // console.log(+dateB);
        // console.log(d);
        // ***********************************


        if(email === null || name === null || password === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        // TODO verify pseudo lenght, mail regex, password etc...
        if(name.lenght >= 13 || name.lenght <= 4){
            return res.status(400).json({'error': 'wrong username (must be lenght 5 - 12)'});
        }

        if(!EMAIL_REGEX.test(email)){
            return res.status(400).json({'error': 'email is not valid'});
        }

        if(!PASSWORD_REGEX.test(password)){
            return res.status(400).json({'error': 'password invalid (must lenght 4 - 12 and include 1 number at least'});
        }

        asyncLib.waterfall([
            function (done) {
                var getUserEmail = 'SELECT email FROM users_by_email WHERE email = ?';
                client.execute(getUserEmail, [email])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify user email'});
                });
            },
            function(result, done){
                if(result.rowLength === 0){
                    done(null, result);
                } else {
                    return res.status(409).json({'error': 'email already exist'});
                }
            },
            function(result, done){
                var getUserName = 'SELECT user_id, name, email FROM users_by_name WHERE name = ?';
                client.execute(getUserName, [name])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify username'});
                });
            },
            function(result, done){
                if (result.rowLength === 0){
                    done(null, result);
                } else {
                    return res.status(409).json({'error': 'name already exist'});
                }
            },
            function(result, done){
                var newUserbyEmail = 'INSERT INTO users_by_email(email, user_id, name, password, isvalidate, isactivity, added_date) VALUES (?, ?, ?, ?, ?, ?, ?)';
                client.execute(newUserbyEmail,[email, id, name, bcryptedPassword, false, false, added_date])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add user line 101'});
                });
            },
            function(result, done){
                var newUserbyName = 'INSERT INTO users_by_name(name, user_id, email, password, isvalidate, isactivity, added_date) VALUES (?, ?, ?, ?, ?, ?, ?)';
                client.execute(newUserbyName,[name, id, email, bcryptedPassword, false, false, added_date])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'cannot add user line 115'});
                });
            },
            function(result, done){
                var newUserbyId = 'INSERT INTO users_by_id(user_id, email, name, password, isvalidate, isactivity, added_date) VALUES (?, ?, ?, ?, ?, ?, ?)';
                client.execute(newUserbyId,[id, email, name, bcryptedPassword, false, false, added_date])
                .then(function(result){
                    done(result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'cannot add user line 129'});
                });
            }
        ], function(result){
            if(result){
                // console.log('added_date: ', added_date);
                var dateBymilisecond = Math.trunc(added_date / 1000);
                // var type = typeof dateBymilisecond;
                // console.log(type);
                // const event = new Date(dateBymilisecond);
                // console.log(event.toUTCString());
                var idUserElastic = id.toString();
                clientElastic.create({
                    id: idUserElastic,
                    index: elasticIndex,
                    body: {
                        activity: 'user',
                        type: 'user',
                        pseudo: name,
                        email: email,
                        addedDate: dateBymilisecond
                    }
                }, (err, result) => {
                    if (err){
                        return res.status(500).json({'error': err});
                    } else {
                        return res.status(201).json({
                            'token': jwtUtils.generateTokenForUser({
                                id: id,
                                email: email,
                                name: name,
                                isValidate: false,
                                isActivity: false,
                                addedDate: dateBymilisecond
                            })
                        });
                    }
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });

    },
    login: function(req, res) {
        // TODO: To implement

        // Params
        var email = req.body.email;
        var password = req.body.password;

        if(email === null || password === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        // TODO verify mail regex, password etc...
        asyncLib.waterfall([
            function(done){
                var getUserEmail = 'SELECT email, password FROM users_by_email WHERE email = ?';
                client.execute(getUserEmail, [email])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify user email'});
                });
            },
            function(result, done){
                if(result.rowLength === 1){
                    bcrypt.compare(password, result.rows[0].password, function(errBycrypt, resBycrypt){
                        done(null, result, resBycrypt);
                    });
                } else {
                    return res.status(404).json({'error': 'user not found'});
                }
            },
            function(result, resBycrypt, done){
                if(resBycrypt){
                    done(null, result);
                } else {
                    return res.status(403).json({'error': 'invalid password'});
                }
            },
            function (result, done) {
                if(result){
                    var getUserId = 'SELECT user_id, email, name, firstname, lastname, phone, isvalidate, isactivity, added_date FROM users_by_email WHERE email = ?';
                    client.execute(getUserId, [email], function (err, result){
                        if (err){
                            return res.status(500).json({'error': 'unable to generate token'});
                        } else {
                            var idToken = result.rows[0].user_id;
                            var emailToken = result.rows[0].email;
                            var nameToken = result.rows[0].name;
                            var firstNameToken = result.rows[0].firstname;
                            var lastNameToken = result.rows[0].lastname;
                            var phoneToken = result.rows[0].phone;
                            var isValidateToken = result.rows[0].isvalidate;
                            var isActivityToken = result.rows[0].isactivity;
                            var addedDateToken = result.rows[0].added_date.getTime(); // date in microsecond from 1970 (timestamp)
                            
                            
                            var dateBymilisecond = Math.trunc(addedDateToken / 1000);
                            // console.log('mathTrunc: ', dateBymilisecond); // date by millisecond from unixDate (1970)
                            // const event = new Date(dateBymilisecond);
                            // console.log('event: ', event.getFullYear());
                            // console.log(event.toUTCString());
                            // var type = typeof event;
                            // console.log('type: ', type);
                            tokenUser = jwtUtils.generateTokenForUser({ /// créer les paires de clefs localsrorage suivant type indicé comme type_name exemple band_name ou orga_name
                                id: idToken,
                                email: emailToken,
                                name: nameToken,
                                isValidate: isValidateToken,
                                isActivity: isActivityToken,
                                addedDate: dateBymilisecond,
                                firstname: firstNameToken,
                                lastname: lastNameToken,
                                phone: phoneToken
                            });
                            createBucket.bucket(idToken.toString());
                            done(tokenUser);
                        }
                    });
                } else {
                    return res.status(500).json({'error': 'cannot log on user'});
                }
            }
        ], function(tokenUser){
                var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
                client.execute(getActivityToken, [email], function (err, result){
                    if (err){
                        return res.status(201).json({
                            'token': tokenUser,
                            'activity': null
                        });
                    } else {
                        if(result.rowLength >= 1){
                            // boucle pour créer la sortie json... 
                            var activity = {};
                            var hasband = false;
                            var hassite = false;
                            var hasmanage = false;
                            var hasorga = false;
                            var hasservice = false;
                            var hasequip = false;
                            for ( var i = 0, len = result.rowLength; i < len; ++i ) {
                                var type = result.rows[i].type;
                                var actname = result.rows[i].actname;
                                var tokenact = result.rows[i].tokenact;
                                var keyToken = type + '_' + actname;
                                var tokenTarget = {};
                                tokenTarget[keyToken] = tokenact;
                                activity = Object.assign(activity, tokenTarget);
                                if (type === 'band') {
                                    hasband = true;
                                }
                                if (type === 'site') {
                                    hassite = true;
                                }
                                if (type === 'manage') {
                                    hasmanage = true;
                                }
                                if (type === 'orga') {
                                    hasorga = true;
                                }
                                if (type === 'service') {
                                    hasservice = true;
                                }
                                if (type === 'equip') {
                                    hasequip = true;
                                }
                            }
                            return res.status(201).json({
                                'token': tokenUser,
                                activity,
                                hasband,
                                hassite,
                                hasmanage,
                                hasorga,
                                hasservice,
                                hasequip
                            });
                            
                        } else {
                            return res.status(201).json({
                                'token': tokenUser,
                                'activity': null
                            });
                        }
                    }
                });
        });

        
    },
    getStatus: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userSatus = jwtUtils.getUserId(headerAuth);
        var userId = userSatus[0];
        var tokenValidDate = userSatus[1];
        var userEmail = userSatus[2];
        // console.log('status: ', userId, tokenValidDate);
        
        userToken = headerAuth.replace('Bearer ', '');

        if (userId < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            } else {
                var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
                client.execute(getActivityToken, [userEmail], function (err, result){
                    if (err){
                        return res.status(201).json({
                            'token': userToken,
                            'activity': null
                        });
                    } else {
                        if(result.rowLength >= 1){
                            // boucle pour créer la sortie json... 
                            var activity = {};
                            var hasband = false;
                            var hassite = false;
                            var hasmanage = false;
                            var hasorga = false;
                            var hasservice = false;
                            var hasequip = false;
                            for ( var i = 0, len = result.rowLength; i < len; ++i ) {
                                var type = result.rows[i].type;
                                var actname = result.rows[i].actname;
                                var tokenact = result.rows[i].tokenact;
                                var keyToken = type + '_' + actname;
                                var tokenTarget = {};
                                tokenTarget[keyToken] = tokenact;
                                activity = Object.assign(activity, tokenTarget);
                                if (type === 'band') {
                                    hasband = true;
                                }
                                if (type === 'site') {
                                    hassite = true;
                                }
                                if (type === 'manage') {
                                    hasmanage = true;
                                }
                                if (type === 'orga') {
                                    hasorga = true;
                                }
                                if (type === 'service') {
                                    hasservice = true;
                                }
                                if (type === 'equip') {
                                    hasequip = true;
                                }
                            }
                            return res.status(201).json({
                                'token': userToken,
                                activity,
                                hasband,
                                hassite,
                                hasmanage,
                                hasorga,
                                hasservice,
                                hasequip
                            });
                            
                        } else {
                            return res.status(201).json({
                                'token': userToken,
                                'activity': null
                            });
                        }
                    }
                });
                // return res.status(201).json({'token': userToken});
            }
            
        }
            

        
    },
    emailConf: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var token = headerAuth.replace('Bearer ', '');
        var userSatus = jwtUtils.getUserConfirmId(token);
        var userId = userSatus[0];
        var userEmail = userSatus[1];
        var userName = userSatus[2];
        

        if (userId < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            // send email with new token

            var newToken = jwtUtils.generateTokenForEmail({
                email: userEmail,
                isValidate: true
                // id: userId,
                // email: userEmail,
                // name: userName,
                // isValidate: true,
                // isActivity: false
            });

            emailUtils.sendEmail(userEmail, userName, newToken, data => {
                if (data.ID === 'ok'){
                    return res.status(201).json({'token': token});
                } else {
                    return res.status(500).json({'error': 'error send'});
                }
                
            });
            
        }
        
    },
    emailLoginConf: function (req, res) {
        var email = req.body.email;
        var password = req.body.password;
        var tokenEmail = req.body.tokenemail;
        var emailStatus = jwtUtils.getTokenEmailLoginConf(tokenEmail);
        var emailStatusEmail = emailStatus[0];
        var emailStatusValidate = emailStatus[1];

        if(email === null || password === null || tokenEmail === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        if (emailStatusValidate === false){
            return res.status(400).json({'error': 'wrong token'});
        }

        if (email != emailStatusEmail){
            return res.status(400).json({'error': 'Your Email adress differ with our Mail'});
        }

        asyncLib.waterfall([
            function(done){
                var getUserEmail = 'SELECT email, password FROM users_by_email WHERE email = ?';
                client.execute(getUserEmail, [email])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify user email'});
                });
            },
            function(result, done){
                if(result.rowLength === 1){
                    bcrypt.compare(password, result.rows[0].password, function(errBycrypt, resBycrypt){
                        done(null, result, resBycrypt);
                    });
                } else {
                    return res.status(404).json({'error': 'user not found'});
                }
            },
            function(result, resBycrypt, done){
                if(resBycrypt){
                    done(null, result);
                } else {
                    return res.status(403).json({'error': 'invalid password'});
                }
            },
            function (result, done) {
                var getUserisValidate = 'SELECT isvalidate FROM users_by_email WHERE email = ?';
                client.execute(getUserisValidate, [email])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify validation in database'});
                });
            },
            function (result, done) {
                if(result.rowLength === 1){
                    var isValidateDB = result.rows[0].isvalidate;
                    if (isValidateDB === true) {
                        return res.status(400).json({'error': 'Your account is already validate'});
                    } else {
                        done(null, result);
                    }
                } else {
                    return res.status(404).json({'error': 'user validation not found'});
                }
            },
            function (result, done) {
                var updateUserByEmail = 'INSERT INTO users_by_email(email, isvalidate) VALUES (?, ?)';
                client.execute(updateUserByEmail,[email, true])
                .then(function(result){
                    done(result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot update users_by_email line 391'});
                });
            }
        ], function(result){
            if(result){
                var getUserId = 'SELECT user_id, email, name, isvalidate, isactivity, added_date FROM users_by_email WHERE email = ?';
                client.execute(getUserId, [email], function (err, result){
                    if (err){
                        return res.status(500).json({'error': 'unable to generate token'});
                    } else {
                        var idToken = result.rows[0].user_id;
                        var emailToken = result.rows[0].email;
                        var nameToken = result.rows[0].name;
                        var isValidateToken = result.rows[0].isvalidate;
                        var isActivityToken = result.rows[0].isactivity;
                        var addedDateToken = result.rows[0].added_date.getTime();
                        
                        var dateBymilisecond = Math.trunc(addedDateToken / 1000);
                        
                        // const event = new Date(dateBymilisecond);
                        // console.log('event: ', event.getFullYear());
                        // console.log(event.toUTCString());
                        // var type = typeof event;
                        // console.log('type: ', type);
                        createBucket.bucket(idToken.toString());
                        return res.status(201).json({
                            'token': jwtUtils.generateTokenForUser({
                                id: idToken,
                                email: emailToken,
                                name: nameToken,
                                isValidate: isValidateToken,
                                isActivity: isActivityToken,
                                addedDate: dateBymilisecond
                            })
                        });
                    }
                });
            } else {
                return res.status(500).json({'error': 'cannot log on user'});
            }
        });




    },
    readuser: function (req, res) {
        var token = req.headers['authorization'];
        var user_id = req.body.userId;

        var userSatus = jwtUtils.getUserId(token);
        const user_userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (user_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        var readUserResult = [];

        asyncLib.waterfall([
            function (done) {
                var getUser = 'SELECT * FROM users_by_id WHERE user_id = ?';
                client.execute(getUser, [user_id])
                .then(function (result) {
                    var dateBymilisecond = Math.trunc(result.rows[0].added_date / 1000);
                    if (result.rows[0].connected === false || result.rows[0].online_set === false || result.rows[0].connected === null || result.rows[0].online_set === null){
                        var onlineStatus = 'Offline';
                    } else {
                        var onlineStatus = 'Online';
                    }
                    var user = {
                        'user': {
                            'userId': result.rows[0].user_id,
                            'name': result.rows[0].name,
                            'email': result.rows[0].email,
                            'addeddate': dateBymilisecond,
                            'activitiesId': result.rows[0].activities_id,
                            'authorStatus': onlineStatus
                        }
                    };
                    readUserResult.push(user);
                    done(null, readUserResult);
                })
                .catch(function (err) {
                    return res.status(500).json({
                        'error': 'unable to get user from user_id'
                    })
                })
            },
            function (result, done) {
                var resultUser = result;
                done(resultUser);
                
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    result
                })
            } else {
                return res.status(500).json({
                    'error': 'unable to get user from user_id'
                })
            }
            
        });

        
    }

}
