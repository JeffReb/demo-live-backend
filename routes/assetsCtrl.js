// env file
require('dotenv').config();
// Imports
var jwtUtils = require('../utils/jwt.utils');
var cassandra = require('cassandra-driver');
var asyncLib = require('async');

// env variables
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;


var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);


var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});

client.connect(function (err, result) {
    console.log('index: cassandra connected to assets');
});

// Routes
module.exports = {
    getImgProfile: function (req, res){

        // Params
        var token = req.headers['authorization'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        var getImgProFileExist = 'SELECT filename FROM assets_by_userid WHERE user_id = ? AND asset_type = ?';
        client.execute(getImgProFileExist, [userid, 'img-profile'])
        .then(function(result){
            if (result.rowLength === 1){
                var Filename = result.rows[0].filename;
                return res.status(201).json({
                    'filename': Filename
                });
            } else {
                return res.status(201).json({
                    'filename': 'ImgProfile not found'
                });
            }
        })
        .catch(function(err) {
            return res.status(500).json({'error': 'unable to verify if ImgProfile Exist'});
        });

    },
    putImgProfile: function (req, res) {
        // console.log('filename', req.file.filename);
        var token = req.headers['authorization'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var asset_id = cassandra.types.TimeUuid.now();
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        asyncLib.waterfall([
            function (done) {
                var putImgProfileDefault = 'INSERT INTO assets_by_userid (user_id, asset_type, filename) VALUES (?, ?, ?)';
                client.execute(putImgProfileDefault,[userid, 'img-profile', req.file.filename])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);
                    return res.status(500).json({'error': 'cannot add asset imgprofiledefault in database'});
                });
            },
            function (result, done) {
                var putImgProfileList = 'INSERT INTO asset_by_userid_and_assetid (user_id, asset_type, asset_id, filename) VALUES (?, ?, ?, ?)';
                client.execute(putImgProfileList,[userid, 'img-profile', asset_id, req.file.filename])
                .then(function(result){
                    done(result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'cannot add asset imgprofilelist in database'});
                });
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    id: req.file.filename
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        // res.send(req.file);
    },
    getImgUser: function (req, res){

        // Params
        var token = req.headers['authorization'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var useridBody = req.body.userId;
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        var getImgProFileExist = 'SELECT filename FROM assets_by_userid WHERE user_id = ? AND asset_type = ?';
        client.execute(getImgProFileExist, [useridBody, 'img-profile'])
        .then(function(result){
            if (result.rowLength === 1){
                var Filename = result.rows[0].filename;
                return res.status(201).json({
                    'filename': Filename
                });
            } else {
                return res.status(201).json({
                    'filename': 'ImgProfile not found'
                });
            }
        })
        .catch(function(err) {
            return res.status(500).json({'error': 'unable to verify if ImgProfile Exist'});
        });

    }
}