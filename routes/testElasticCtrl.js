// env file
require('dotenv').config();
// Imports

const { Client } = require('@elastic/elasticsearch');

// env variables
const elasticHost = process.env.ELASTIC_HOST;
const elasticIndex = process.env.ELASTIC_INDEX;
const elascticHostadress = elasticHost + ':9200';


console.log('elascticHostadress', elascticHostadress);

const client = new Client({ node: elascticHostadress });

module.exports = {
    getRebaudSearch: function (req, res) {
        var nameFilter = req.body.namefilter;
        var name = req.body.name;
        var activityFilter = req.body.activityfilter;
        var activities = req.body.activities;
        var typeFilter = req.body.typefilter;
        var type = req.body.type;
        var styleFilter = req.body.stylefilter;
        var style = req.body.style;
        var locationFilter = req.body.locationfilter;
        var locationDistance = req.body.locationdistance;
        var locationLat = req.body.locationlat;
        var locationLon = req.body.locationlon;

        var locationLatFloat = parseFloat(locationLat);
        var locationLonFloat = parseFloat(locationLon);

        if (nameFilter === 'false' && activityFilter === 'false' && styleFilter === 'false') {
            var queryBodyMust = [
                {
                    match: {
                        activity: 'user'
                    }
                }
            ];
        } else {
            var queryBodyMust = [];
            if (nameFilter === 'true'){
                var queryBodyMustName = [
                    {
                        prefix: {
                            name: name
                        }
                    }
                ];
                queryBodyMust.push(queryBodyMustName);
            }
            if (activityFilter === 'true'){
                var queryBodyMustAct = [
                    {
                        match: {
                            activity: activities
                        }
                    }
                ];
                queryBodyMust.push(queryBodyMustAct);
            }
            if (typeFilter === 'true'){
                var queryBodyMustType = [
                    {
                        match: {
                            service: type
                        }
                    }
                ];
                queryBodyMust.push(queryBodyMustType);
            }
            if (styleFilter === 'true'){
                var queryBodyMustStyle = [
                    {
                        match: {
                            style: style
                        }
                    }
                ];
                queryBodyMust.push(queryBodyMustStyle);
            }
        }

        if (locationFilter === 'true'){
            var queryBodyFilter = [
                {
                    'geo_distance': {
                        'distance': locationDistance,
                        'location': {
                            'lat': locationLatFloat,
                            'lon': locationLonFloat
                        }
                    } 
                }
            ];
            if (nameFilter === 'false' && activityFilter === 'false' && styleFilter === 'false'){
                var queryBody = {
                    'bool': {
                        must_not: queryBodyMust,
                        filter: queryBodyFilter
                    }
                };
            } else {
                var queryBody = {
                    'bool': {
                        must: queryBodyMust,
                        filter: queryBodyFilter
                    }
                };
            }
        } else {
            if (nameFilter === 'false' && activityFilter === 'false' && styleFilter === 'false'){
                var queryBody = {
                    'bool': {
                        must_not: queryBodyMust
                    }
                };

            } else {
                var queryBody = {
                    'bool': {
                        must: queryBodyMust
                    }
                };
            }
        }
  
        client.search({
            from: 0,
            size: 5000,
            index: elasticIndex,
            body: { 
                sort: [{'addedDate': {'order': 'desc'}}],
                query: queryBody
             }
          }, (err, result) => {
            if (err) {
                // console.log('elascticHostadress', elascticHostadress);
                return res.status(500).json({'error': err});
            } else {
                // console.log('elascticHostadress', elascticHostadress);
                return res.status(201).json({'result': result.body.hits.hits});
            }
          })

    }
}