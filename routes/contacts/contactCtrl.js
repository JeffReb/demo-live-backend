// env file
require('dotenv').config();

// imports
var jwtUtils = require('../../utils/jwt.utils');
var cassandra = require('cassandra-driver');
var asyncLib = require('async');
var sendNotif = require('../../utils/notification.utils');

// env variables cassandra
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;

var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);

var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace, 
    authProvider: authProvider
});

client.connect(function (err, result) {
    console.log('index: cassandra connected to contacts');
});


module.exports = {
    sendReqContact: function (req, res) { // Me I send request invitation to contact user Destination (Dest)

        // Params
        var token = req.headers['authorization'];
        var contactId = req.body.contactId; // user destination id

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0]; //     my id
        var tokenValidDate = userSatus[1];

        const valueSetTypeDest = [ // user destination id
            contactId
        ];

        const valueSetTypeMe = [ //     my id
            userid
        ];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }
        
        asyncLib.waterfall([
            function (done) {
                var sendContactToDest = 'UPDATE users_by_id SET pending_contacts=pending_contacts + ? WHERE user_id=?'; // send my id request to destination user
                client.execute(sendContactToDest,[valueSetTypeMe, contactId], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to send my id to Destination'});
                });
            },
            function (result, done) {
                var sendContactToMe = 'UPDATE users_by_id SET pending_contacts=pending_contacts + ? WHERE user_id=?'; // send to me the id of the destination user
                client.execute(sendContactToMe,[valueSetTypeDest, userid], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to send idB to Me'});
                });
            },
            function (result, done) {
                var sendContactToValidateToDest = 'UPDATE users_by_id SET valid_contacts=valid_contacts + ? WHERE user_id=?'; // send my id request to destination user for validation
                client.execute(sendContactToValidateToDest,[valueSetTypeMe, contactId], { prepare: true })
                .then(function (result) {
                    done(result);                    
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to send my id to B for validation'});
                });
            }
        ], function (result) {
            if(result){
                var notification = sendNotif.sendNotificationContact(contactId, userid, 'invitation');
                // console.log('notification: ', notification);
                if (notification === 'false') {
                    return res.status(201).json({
                        'ok': 'send contact request without notification'
                    });
                } else {
                    return res.status(201).json({
                        'ok': 'send contact request with notification'
                    });
                }
                
            } else {
                return res.status(500).json({'error': 'cannot send request'});
            }
        });
    },
    acceptInvitationContact: function (req, res) { // Me I accept invitation from contact user Destination (Dest)

        // Params
        var token = req.headers['authorization'];
        var contactId = req.body.contactId; // user destination id

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0]; //     my id
        var tokenValidDate = userSatus[1];

        const valueSetTypeDest = [ // user destination id
            contactId
        ];

        const valueSetTypeMe = [ //     my id
            userid
        ];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }
        
        asyncLib.waterfall([
            function (done) {
                var deleteContactPendingToDest = 'UPDATE users_by_id SET pending_contacts=pending_contacts - ? WHERE user_id=?'; // delete my Id to pending Dest
                client.execute(deleteContactPendingToDest,[valueSetTypeMe, contactId], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to delete my id to Dest Pending'});
                });
            },
            function (result, done) {
                var deleteContactPendingToMe = 'UPDATE users_by_id SET pending_contacts=pending_contacts - ? WHERE user_id=?'; // delete Dest Id in my pending
                client.execute(deleteContactPendingToMe,[valueSetTypeDest, userid], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to delete id Dest to My Pending'});
                });
            },
            function (result, done) {
                var deleteContactValidToMe = 'UPDATE users_by_id SET valid_contacts=valid_contacts - ? WHERE user_id=?'; // delete Dest Id in my valid
                client.execute(deleteContactValidToMe,[valueSetTypeDest, userid], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to delete id Dest to my Valid'});
                });
            },
            function (result, done) {
                var addContactToMe = 'UPDATE users_by_id SET confirm_contacts=confirm_contacts + ? WHERE user_id=?'; // add Dest Id in my Contact
                client.execute(addContactToMe,[valueSetTypeDest, userid], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to send idB to Me'});
                });
            },
            function (result, done) {
                var addContactToDest = 'UPDATE users_by_id SET confirm_contacts=confirm_contacts + ? WHERE user_id=?'; // add My Id to Dest contact
                client.execute(addContactToDest,[valueSetTypeMe, contactId], { prepare: true })
                .then(function (result) {
                    done(result);                    
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to send my id to contact Dest'});
                });
            }
        ], function (result) {
            if(result){
                var notification = sendNotif.sendNotificationContact(contactId, userid, 'accept');
                // console.log('notification: ', notification);
                if (notification === 'false') {
                    return res.status(201).json({
                        'ok': 'send contact acceptation without notification'
                    });
                } else {
                    return res.status(201).json({
                        'ok': 'send contact acceptation with notification'
                    });
                }
                
            } else {
                return res.status(500).json({'error': 'cannot send acceptation'});
            }
        });
    },
    refuseInvitationContact: function (req, res) { // Me I refuse invitation from contact user Destination (Dest)

        // Params
        var token = req.headers['authorization'];
        var contactId = req.body.contactId; // user destination id

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0]; //     my id
        var tokenValidDate = userSatus[1];

        const valueSetTypeDest = [ // user destination id
            contactId
        ];

        const valueSetTypeMe = [ //     my id
            userid
        ];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }
        
        asyncLib.waterfall([
            function (done) {
                var deleteContactPendingToDest = 'UPDATE users_by_id SET pending_contacts=pending_contacts - ? WHERE user_id=?'; // delete my Id to pending Dest
                client.execute(deleteContactPendingToDest,[valueSetTypeMe, contactId], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log('l244', err);
                    return res.status(500).json({'error': 'unable to delete my id to Dest Pending'});
                });
            },
            function (result, done) {
                var deleteContactPendingToMe = 'UPDATE users_by_id SET pending_contacts=pending_contacts - ? WHERE user_id=?'; // delete Dest Id in my pending
                client.execute(deleteContactPendingToMe,[valueSetTypeDest, userid], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log('l255', err);
                    return res.status(500).json({'error': 'unable to delete id Dest to My Pending'});
                });
            },
            function (result, done) {
                var deleteContactValidToMe = 'UPDATE users_by_id SET valid_contacts=valid_contacts - ? WHERE user_id=?'; // delete Dest Id in my valid
                client.execute(deleteContactValidToMe,[valueSetTypeDest, userid], { prepare: true })
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log('l266', err);
                    return res.status(500).json({'error': 'unable to delete id Dest to my Valid'});
                });
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    'ok': 'refuse request'
                });
            } else {
                return res.status(500).json({'error': 'cannot send request'});
            }
        });
    },
    deleteContact: function (req, res) { // Me I delete a contact

        // Params
        var token = req.headers['authorization'];
        var contactId = req.body.contactId; // user destination id

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0]; //     my id
        var tokenValidDate = userSatus[1];

        const valueSetTypeDest = [ // user destination id
            contactId
        ];

        const valueSetTypeMe = [ //     my id
            userid
        ];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }
        
        asyncLib.waterfall([
            function (done) {
                var addContactToMe = 'UPDATE users_by_id SET confirm_contacts=confirm_contacts - ? WHERE user_id=?'; // delete Dest Id in my Contact
                client.execute(addContactToMe,[valueSetTypeDest, userid], { prepare: true })
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to delete Dest Id in my contact'});
                });
            },
            function (result, done) {
                var addContactToDest = 'UPDATE users_by_id SET confirm_contacts=confirm_contacts - ? WHERE user_id=?'; // delete My Id to Dest contact
                client.execute(addContactToDest,[valueSetTypeMe, contactId], { prepare: true })
                .then(function (result) {
                    done(result);                    
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to delete my Id to Dest Contact'});
                });
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    'ok': 'contact deleting'
                });
                
            } else {
                return res.status(500).json({'error': 'cannot send request'});
            }
        });
    },
    contactStateByID: function (req, res) {


        // Params
        var token = req.headers['authorization'];
        var contactId = req.body.contactId;
        

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var getStateContactisToValidate = 'SELECT valid_contacts FROM users_by_id WHERE user_id = ?';
                client.execute(getStateContactisToValidate,[userid])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    return res.status(500).json({'error': 'unable to verify userid'});
                });
            },
            function (result, done) {
                if(result.rowLength === 0){
                    done(null, result);
                } else {
                    var validate = result.rows[0]['valid_contacts'];
                    if(validate === null){
                        done(null, result);
                    } else {
                        const findId = validate.indexOf(contactId);
                        if (findId < 0) {
                            done(null, result);
                        } else {
                            return res.status(201).json({'contactstate': 'valid'});
                        }
                    }
                }
            },
            function (result, done) {
                var getStateContactisPending = 'SELECT pending_contacts FROM users_by_id WHERE user_id = ?';
                client.execute(getStateContactisPending,[userid])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    return res.status(500).json({'error': 'unable to verify userid'});
                });
            },
            function (result, done) {
                if(result.rowLength === 0){
                    done(null, result);
                } else {
                    var pending = result.rows[0]['pending_contacts'];
                    if (pending === null){
                        done(null, result);
                    } else {
                        const findId = pending.indexOf(contactId);
                        if (findId < 0){
                            done(null, result);
                        } else {
                            return res.status(201).json({'contactstate': 'pending'});
                        }
                    }
                }
            },
            function (result, done) {
                var getStateContactisConfirmed = 'SELECT confirm_contacts FROM users_by_id WHERE user_id = ?';
                client.execute(getStateContactisConfirmed,[userid])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    return res.status(500).json({'error': 'unable to verify userid'});
                });
            },
            function (result, done) {
                if (result.rowLength === 0){
                    done(result);
                } else {
                    var confirmed = result.rows[0]['confirm_contacts'];
                    if (confirmed === null){
                        done(result);
                    } else {
                        const findId = confirmed.indexOf(contactId);
                        if (findId < 0){
                            done(result);
                        } else {
                            return res.status(201).json({'contactstate': 'confirmed'});
                        }
                    }
                }
            }
        ], function (result) {
            return res.status(201).json({'contactstate': 'available'});
        });
        
    },
    listingContactByID: function (req, res) {


        // Params
        var token = req.headers['authorization'];
        
        

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var listContact = 'SELECT confirm_contacts FROM users_by_id WHERE user_id = ?';
                client.execute(listContact,[userid])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot read user to listing contact'});
                })
            },
            function (result, done) {
                if(result.rowLength === 0){
                    var resultat = [];
                    return res.status(201).json({
                        'contactsId': resultat
                    });
                } else {
                    done(result);
                }
            }
        ], function (result) {
            if (result){
                var resultat = result.rows[0].confirm_contacts;
                return res.status(201).json({
                    'contactsId': resultat
                });
            } else {
                return res.status(500).json({'error': 'cannot read contact in users_by_id'});
            }
        });
        
    }
}