// env file
require('dotenv').config();

// imports
var jwtUtils = require('../../utils/jwt.utils');
var cassandra = require('cassandra-driver');
var asyncLib = require('async');
var sendNotif = require('../../utils/notification.utils');

// env variables cassandra
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;

var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);

var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace, 
    authProvider: authProvider
});

client.connect(function (err, result) {
    console.log('index: cassandra connected to message');
});

module.exports = {
    sendMessage: function (req, res) {
        // Params
        var token = req.headers['authorization'];
        var contactId = req.body.contactId; // user destination id
        var message = req.body.message; // message content

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0]; //     my id
        var tokenValidDate = userSatus[1];

        var messageId = cassandra.types.TimeUuid.now();
        var added_date = cassandra.types.generateTimestamp();

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var sendMessageToMe = 'INSERT INTO users_by_id_and_messchat_id(user_id, message_id, user_contact_id, flux_type, message, added_date) VALUES (?, ?, ?, ?, ?, ?)';
                client.execute(sendMessageToMe,[userid, messageId, contactId, 'send', message, added_date])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot add message to me'});
                })
            },
            function (result, done) {
                var sendMessageToYou = 'INSERT INTO users_by_id_and_messchat_id(user_id, message_id, user_contact_id, flux_type, message, added_date) VALUES (?, ?, ?, ?, ?, ?)';
                client.execute(sendMessageToYou,[contactId, messageId, userid, 'receive', message, added_date])
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot add message to you'});
                })
            }
        ], function (result) {
            if(result){
                var notification = sendNotif.sendNotificationMessage(contactId, userid);
                // console.log('notification: ', notification);
                if (notification === 'false') {
                    return res.status(201).json({
                        'ok': 'send message without notification'
                    });
                } else {
                    return res.status(201).json({
                        'ok': 'send message with notification'
                    });
                }
            } else {
                return res.status(500).json({'error': 'cannot send message'});
            }
        });
        
    },
    receiveMessage: function (req, res) {
        // Params
        var token = req.headers['authorization'];
        

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0]; //     my id
        var tokenValidDate = userSatus[1];

        

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var listMessage = 'SELECT * FROM users_by_id_and_messchat_id WHERE user_id = ?';
                client.execute(listMessage,[userid])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot read message to me'});
                })
            },
            function (result, done) {
                if(result.rowLength === 0){
                    var resultat = [];
                    return res.status(201).json({
                        'messages': resultat
                    });
                } else {
                    done(result);
                }
            }
        ], function (result) {
            if(result){
                var resultat = [];
                var resultatinside = {};
                var resultatLength = result.rowLength;
                for ( var i = 0, len = resultatLength; i < len; ++i ){
                    var dateBymilisecond = Math.trunc(result.rows[i].added_date / 1000);
                    resultatinside = {
                        userId: result.rows[i].user_id,
                        messageId: result.rows[i].message_id,
                        userContactId: result.rows[i].user_contact_id,
                        fluxType: result.rows[i].flux_type,
                        message: result.rows[i].message,
                        addedDate: dateBymilisecond
                    }
                    resultat.push(resultatinside);
                }
                return res.status(201).json({
                    'messages': resultat
                });
            } else {
                return res.status(500).json({'error': 'cannot read message'});
            }
        });
        
    },
    setChatWith: function (req, res) {
        // Params
        var token = req.headers['authorization'];
        var contactId = req.body.contactId; // user correspondant
        

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0]; //     my id
        var tokenValidDate = userSatus[1];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var setChatWith = 'UPDATE users_by_id SET chat_with=? WHERE user_id=?';
                client.execute(setChatWith,[contactId, userid])
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot add my correspondant id'});
                })
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    'ok': 'correspondant save'
                });
            } else {
                return res.status(500).json({'error': 'cannot save correspondant'});
            }
        });
        
    },
    removeChatWith: function (req, res) {
        // Params
        var token = req.headers['authorization'];
        
        

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0]; //     my id
        var tokenValidDate = userSatus[1];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var setChatWith = 'UPDATE users_by_id SET chat_with=? WHERE user_id=?';
                client.execute(setChatWith,[null, userid])
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot remove my correspondant id'});
                })
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    'ok': 'correspondant remove'
                });
            } else {
                return res.status(500).json({'error': 'cannot remove correspondant'});
            }
        });
        
    }
}