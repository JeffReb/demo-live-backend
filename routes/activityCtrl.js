// env file
require('dotenv').config();
// Imports
var bcrypt = require('bcrypt');
var jwtUtils = require('../utils/jwt.utils');
var cassandra = require('cassandra-driver');
var asyncLib = require('async');
var geohash = require('ngeohash');
var createBucket = require('../utils/upload/createBucket');
const { Client } = require('@elastic/elasticsearch');

// env variables cassandra
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;

// env variables elastic
const elasticHost = process.env.ELASTIC_HOST;
const elasticIndex = process.env.ELASTIC_INDEX;
const elascticHostadress = elasticHost + ':9200';



var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);


var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});

const clientElastic = new Client({ node: elascticHostadress });


client.connect(function (err, result) {
    console.log('index: cassandra connected to activity');
});

const Mapper = cassandra.mapping.Mapper;

// Constants
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/


// Routes
module.exports = {
    registerband: function (req, res) {
        // TODO: To implement
 
        

        // Params
        var token = req.headers['authorization'];
        var band_id = cassandra.types.TimeUuid.now();
        var band_name = req.body.nameband;
        var stylea = req.body.style1;
        var styleb = req.body.style2;
        var stylec = req.body.style3;
        var style = [stylea]; // { '2013-9-22 12:01' : 'birthday wishes to Bilbo', '2013-10-1 18:00': 'Check into Inn of Pracing Pony'}
        var band_email = req.body.emailband;
        var band_role = req.body.roleband;
        var band_cityid = req.body.cityid;
        var band_city = req.body.city;
        var band_cityalt2id = req.body.cityalt2id;
        var band_cityalt2 = req.body.cityalt2;
        var band_cityalt1id = req.body.cityalt1id;
        var band_cityalt1 = req.body.cityalt1;
        var band_countryid = req.body.countryid;
        var band_country = req.body.country;
        var band_countrycode = req.body.countrycode;
        var band_continent = req.body.continent;
        var band_latitude = req.body.latBand;
        var band_longitude = req.body.lngBand;
        
        var band_added_date = cassandra.types.generateTimestamp();
        // var str1 = 'Bearer';
        // var token = str1.concat(' ', req.body.token);


        var latitude = eval(band_latitude);
        var longitude = eval(band_longitude);
        

        var band_geohash = geohash.encode(latitude, longitude);

        if (styleb){
            
            style.push(styleb);
        }

        if (stylec){
            style.push(stylec);
        }
        

        
        


       

        var userSatus = jwtUtils.getUserId(token);
        const band_userid = userSatus[0];
        var tokenValidDate = userSatus[1];
        var userEmail = userSatus[2];

        var tokenband = jwtUtils.generateTokenForBand({
            band_id: band_id,
            band_name: band_name,
            style: style,
            band_email: band_email,
            band_role: band_role
        });
        

        if (band_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        if(band_name === null || stylea === null || band_cityid === null || band_role === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        

        if(band_name.lenght >= 13 || band_name.lenght <= 3){
            return res.status(400).json({'error': 'wrong bandname (must be lenght 4 - 12)'});
        }

        

        // if(!band_email){
        //     var band_email = null;
        // } else {
        //     if(!EMAIL_REGEX.test(band_email)){
        //         return res.status(400).json({'error': 'email is not valid'});
        //     }
        // }

        // if(!EMAIL_REGEX.test(band_email)){
        //     return res.status(400).json({'error': 'email is not valid'});
        // }

        

        asyncLib.waterfall([
            function (done) {
                var getBandExist = 'SELECT * FROM bands_by_cityalt2_and_name WHERE band_cityalt2id = ? AND band_name = ?';
                client.execute(getBandExist, [band_cityalt2id, band_name])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify name and cityalt2id of band: line 139'});
                });
            },
            function(result, done){
                if(result.rowLength === 0){
                    done(null, result);
                } else {
                    var resultTxta = 'This band already exist in the district of '.concat(band_cityalt2);
                    var resultTxtb = resultTxta.concat(' in ');
                    var resultTxt = resultTxtb.concat(band_cityalt1);
                    return res.status(409).json({'error': resultTxt});
                }
            },
            function(result, done){
                var newBandById = 'INSERT INTO bands_by_id(band_id, band_name, style, band_email, role, band_cityid, band_city, band_cityalt2id, band_cityalt2, band_cityalt1id, band_cityalt1, band_countryid, band_country, band_countrycode, band_continent, band_latitude, band_longitude, band_geohash, band_added_date, band_userid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                client.execute(newBandById,[
                    band_id,
                    band_name,
                    style,
                    band_email,
                    band_role,
                    band_cityid,
                    band_city,
                    band_cityalt2id,
                    band_cityalt2,
                    band_cityalt1id,
                    band_cityalt1,
                    band_countryid,
                    band_country,
                    band_countrycode,
                    band_continent,
                    band_latitude,
                    band_longitude,
                    band_geohash,
                    band_added_date,
                    band_userid
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add bands_by_id: line 179'});
                });
            },
            function(result, done){
                var newBandById = 'INSERT INTO bands_by_cityalt2_and_name(band_id, band_name, band_cityalt2id) VALUES (?, ?, ?)';
                client.execute(newBandById,[
                    band_id,
                    band_name,
                    band_cityalt2id
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add bands_by_cityalt2_and_name: line 212'});
                });
            },
            function (result, done) {
                var newUserbyEmailBandName = 'INSERT INTO users_by_email_and_typeactname(email, type, actname, tokenact) VALUES (?, ?, ?, ?)';
                client.execute(newUserbyEmailBandName,[
                    userEmail,
                    'band',
                    band_name,
                    tokenband
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add users_by_email_and_bandname: line 225'});
                });
            },
            function (result, done) {
                const bandIdStr = band_id.toString();
                const bandIdTable = [
                    bandIdStr
                ];
                var queryBandIdToUserId = 'UPDATE users_by_id SET activities_id=activities_id + ? WHERE user_id=?';
                client.execute(queryBandIdToUserId,[bandIdTable, band_userid], { prepare: true })
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add activity band_id in user_by_id'});
                })
            }
        ], function(result){
            if(result){

                //Elastic Search create band
                var dateBymilisecond = Math.trunc(band_added_date / 1000);
                var idUserElastic = band_userid.toString();
                var idBandElastic = band_id.toString();
                clientElastic.create({
                    id: idBandElastic,
                    index: elasticIndex,
                    body: {
                        activity: 'band',
                        userid: idUserElastic,
                        name: band_name,
                        type: 'band',
                        style: style,
                        city: band_city,
                        region: band_cityalt1,
                        dept: band_cityalt2,
                        country: band_country,
                        countrycode: band_countrycode,
                        continent: band_continent,
                        location: {
                            lat: band_latitude,
                            lon: band_longitude
                        },
                        addedDate: dateBymilisecond
                    }
                }, (err, result) => {
                    if (err) {
                        return res.status(500).json({'error': err});
                    } else {
                        var tokenWhitoutBear = token.replace('Bearer ', '');
                        var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
                        client.execute(getActivityToken, [userEmail], function (err, resultactivity){ // only for update activity here (not in front)
                            if (err){
                                return res.status(201).json({
                                    'token': tokenWhitoutBear,
                                    'activity': 'unable to find activity'
                                });
                            } else {
                                if(resultactivity.rowLength >= 1){
                                    // boucle pour créer la sortie json... 
                                    var activity = {};
                                    var hasband = false;
                                    var hassite = false;
                                    var hasmanage = false;
                                    var hasorga = false;
                                    var hasservice = false;
                                    var hasequip = false;
                                    for ( var i = 0, len = resultactivity.rowLength; i < len; ++i ) {
                                        var type = resultactivity.rows[i].type;
                                        var actname = resultactivity.rows[i].actname;
                                        var tokenact = resultactivity.rows[i].tokenact;
                                        var keyToken = type + '_' + actname;
                                        var tokenTarget = {};
                                        tokenTarget[keyToken] = tokenact;
                                        activity = Object.assign(activity, tokenTarget);
                                        if (type === 'band') {
                                            hasband = true;
                                        }
                                        if (type === 'site') {
                                            hassite = true;
                                        }
                                        if (type === 'manage') {
                                            hasmanage = true;
                                        }
                                        if (type === 'orga') {
                                            hasorga = true;
                                        }
                                        if (type === 'service') {
                                            hasservice = true;
                                        }
                                        if (type === 'equip') {
                                            hasequip = true;
                                        }
                                    }
                                    return res.status(201).json({
                                        'token': tokenWhitoutBear,
                                        activity,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                    
                                } else {
                                    return res.status(201).json({
                                        'token': token,
                                        'activity': null,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                }
                            }
                        });
                        // return res.status(201).json({
                        //     'token' : token,
                        //     'activity': tokenband
                        // });

                    }
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    registersite: function (req, res) {
        // TODO: To implement
 
        

        // Params
        var token = req.headers['authorization'];
        var site_id = cassandra.types.TimeUuid.now();
        var site_name = req.body.namehall;
        var site_adress = req.body.adresshall;
        var site_phone = req.body.phonehall;
        var site_email = req.body.emailhall;
        var site_type = req.body.typehall;
        var site_capacity = req.body.capacityhall;
        var site_cityid = req.body.cityid;
        var site_city = req.body.city;
        var site_cityalt2id = req.body.cityalt2id;
        var site_cityalt2 = req.body.cityalt2;
        var site_cityalt1id = req.body.cityalt1id;
        var site_cityalt1 = req.body.cityalt1;
        var site_countryid = req.body.countryid;
        var site_country = req.body.country;
        var site_countrycode = req.body.countrycode;
        var site_continent = req.body.continent;
        var site_latitude = req.body.latHall;
        var site_longitude = req.body.lngHall;
        var site_stylea = req.body.stylehall;
        var site_style = [site_stylea];
        
        var site_added_date = cassandra.types.generateTimestamp();
        // var str1 = 'Bearer';
        // var token = str1.concat(' ', req.body.token);


        var latitude = eval(site_latitude);
        var longitude = eval(site_longitude);
        

        var site_geohash = geohash.encode(latitude, longitude);

        var userSatus = jwtUtils.getUserId(token);
        const site_userid = userSatus[0];
        var tokenValidDate = userSatus[1];
        var userEmail = userSatus[2];

        var tokensite = jwtUtils.generateTokenForsite({
            site_id: site_id,
            site_name: site_name,
            site_type: site_type,
            site_email: site_email,
            site_capacity: site_capacity
        });
        

        if (site_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        if(site_name === null || site_type === null || site_cityid === null || site_capacity === null || site_email === null || site_phone === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        

        if(site_name.lenght >= 13 || site_name.lenght <= 3){
            return res.status(400).json({'error': 'wrong sitename (must be lenght 4 - 12)'});
        }

        

        // if(!band_email){
        //     var band_email = null;
        // } else {
        //     if(!EMAIL_REGEX.test(band_email)){
        //         return res.status(400).json({'error': 'email is not valid'});
        //     }
        // }

        // if(!EMAIL_REGEX.test(band_email)){
        //     return res.status(400).json({'error': 'email is not valid'});
        // }

        

        asyncLib.waterfall([
            function(done){
                var newSiteById = 'INSERT INTO sites_by_id(site_id, site_name, site_adress, site_phone, site_type, site_email, site_capacity, site_cityid, site_city, site_cityalt2id, site_cityalt2, site_cityalt1id, site_cityalt1, site_countryid, site_country, site_countrycode, site_continent, site_latitude, site_longitude, site_geohash, site_added_date, site_userid, site_styles) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                client.execute(newSiteById,[
                    site_id,
                    site_name,
                    site_adress,
                    site_phone,
                    site_type,
                    site_email,
                    site_capacity,
                    site_cityid,
                    site_city,
                    site_cityalt2id,
                    site_cityalt2,
                    site_cityalt1id,
                    site_cityalt1,
                    site_countryid,
                    site_country,
                    site_countrycode,
                    site_continent,
                    site_latitude,
                    site_longitude,
                    site_geohash,
                    site_added_date,
                    site_userid,
                    site_stylea
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add sites_by_id: line 179'});
                });
            },
            function (result, done) {
                var newUserbyEmailSiteName = 'INSERT INTO users_by_email_and_typeactname(email, type, actname, tokenact) VALUES (?, ?, ?, ?)';
                client.execute(newUserbyEmailSiteName,[
                    userEmail,
                    'site',
                    site_name,
                    tokensite
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add users_by_email_and_sitename: line 225'});
                });
            },
            function (result, done) {
                const siteIdStr = site_id.toString();
                const siteIdTable = [
                    siteIdStr
                ];
                var querySiteIdToUserId = 'UPDATE users_by_id SET activities_id=activities_id + ? WHERE user_id=?';
                client.execute(querySiteIdToUserId,[siteIdTable, site_userid], { prepare: true })
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add activity site_id in user_by_id'});
                })
            }
        ], function(result){
            if(result){

                //Elastic Search create site
                var dateBymilisecond = Math.trunc(site_added_date / 1000);
                var idUserElastic = site_userid.toString();
                var idSiteElastic = site_id.toString();

                clientElastic.create({
                    id: idSiteElastic,
                    index: elasticIndex,
                    body: {
                        activity: 'site',
                        userid: idUserElastic,
                        name: site_name,
                        type: site_type,
                        style: site_style,
                        capacity: site_capacity,
                        city: site_city,
                        region: site_cityalt1,
                        dept: site_cityalt2,
                        country: site_country,
                        countrycode: site_countrycode,
                        continent: site_continent,
                        location: {
                            lat: site_latitude,
                            lon: site_longitude
                        },
                        addedDate: dateBymilisecond
                    }
                }, (err, result) => {
                    if (err) {
                        return res.status(500).json({'error': err});
                    } else {
                        var tokenWhitoutBear = token.replace('Bearer ', '');
                        var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
                        client.execute(getActivityToken, [userEmail], function (err, resultactivity){ // only for update activity here (not in front)
                            if (err){
                                return res.status(201).json({
                                    'token': tokenWhitoutBear,
                                    'activity': 'unable to find activity'
                                });
                            } else {
                                if(resultactivity.rowLength >= 1){
                                    // boucle pour créer la sortie json... 
                                    var activity = {};
                                    var hasband = false;
                                    var hassite = false;
                                    var hasmanage = false;
                                    var hasorga = false;
                                    var hasservice = false;
                                    var hasequip = false;
                                    for ( var i = 0, len = resultactivity.rowLength; i < len; ++i ) {
                                        var type = resultactivity.rows[i].type;
                                        var actname = resultactivity.rows[i].actname;
                                        var tokenact = resultactivity.rows[i].tokenact;
                                        var keyToken = type + '_' + actname;
                                        var tokenTarget = {};
                                        tokenTarget[keyToken] = tokenact;
                                        activity = Object.assign(activity, tokenTarget);
                                        if (type === 'band') {
                                            hasband = true;
                                        }
                                        if (type === 'site') {
                                            hassite = true;
                                        }
                                        if (type === 'manage') {
                                            hasmanage = true;
                                        }
                                        if (type === 'orga') {
                                            hasorga = true;
                                        }
                                        if (type === 'service') {
                                            hasservice = true;
                                        }
                                        if (type === 'equip') {
                                            hasequip = true;
                                        }
                                    }
                                    return res.status(201).json({
                                        'token': tokenWhitoutBear,
                                        activity,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                    
                                } else {
                                    return res.status(201).json({
                                        'token': token,
                                        'activity': null,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                }
                            }
                        });
                        // return res.status(201).json({
                        //     'token' : token,
                        //     'activity': tokenband
                        // });
                    }
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    registermanage: function (req, res) {
        // TODO: To implement
 
        

        // Params
        var token = req.headers['authorization'];
        var manage_id = cassandra.types.TimeUuid.now();
        var manage_name = req.body.namemanage;
        var manage_adress = req.body.adressmanage;
        var manage_phone = req.body.phonemanage;
        var manage_email = req.body.emailmanage;
        var manage_type = req.body.typemanage;
        var manage_status = req.body.statusmanage;
        var manage_cityid = req.body.cityid;
        var manage_city = req.body.city;
        var manage_cityalt2id = req.body.cityalt2id;
        var manage_cityalt2 = req.body.cityalt2;
        var manage_cityalt1id = req.body.cityalt1id;
        var manage_cityalt1 = req.body.cityalt1;
        var manage_countryid = req.body.countryid;
        var manage_country = req.body.country;
        var manage_countrycode = req.body.countrycode;
        var manage_continent = req.body.continent;
        var manage_latitude = req.body.latManage;
        var manage_longitude = req.body.lngManage;
        var manage_stylea = req.body.stylemanage;
        var manage_style = [manage_stylea];
        
        var manage_added_date = cassandra.types.generateTimestamp();
        // var str1 = 'Bearer';
        // var token = str1.concat(' ', req.body.token);


        var latitude = eval(manage_latitude);
        var longitude = eval(manage_longitude);
        

        var manage_geohash = geohash.encode(latitude, longitude);

        var userSatus = jwtUtils.getUserId(token);
        const manage_userid = userSatus[0];
        var tokenValidDate = userSatus[1];
        var userEmail = userSatus[2];

        var tokenmanage = jwtUtils.generateTokenFormanage({
            manage_id: manage_id,
            manage_name: manage_name,
            manage_type: manage_type,
            manage_email: manage_email,
            manage_status: manage_status
        });
        

        if (manage_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        if(manage_name === null || manage_type === null || manage_cityid === null || manage_status === null || manage_email === null || manage_phone === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        

        if(manage_name.lenght >= 13 || manage_name.lenght <= 3){
            return res.status(400).json({'error': 'wrong managename (must be lenght 4 - 12)'});
        }

        

        // if(!band_email){
        //     var band_email = null;
        // } else {
        //     if(!EMAIL_REGEX.test(band_email)){
        //         return res.status(400).json({'error': 'email is not valid'});
        //     }
        // }

        // if(!EMAIL_REGEX.test(band_email)){
        //     return res.status(400).json({'error': 'email is not valid'});
        // }

        

        asyncLib.waterfall([
            function(done){
                var newManageById = 'INSERT INTO manages_by_id(manage_id, manage_name, manage_adress, manage_phone, manage_type, manage_email, manage_status, manage_cityid, manage_city, manage_cityalt2id, manage_cityalt2, manage_cityalt1id, manage_cityalt1, manage_countryid, manage_country, manage_countrycode, manage_continent, manage_latitude, manage_longitude, manage_geohash, manage_added_date, manage_userid, manage_styles) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                client.execute(newManageById,[
                    manage_id,
                    manage_name,
                    manage_adress,
                    manage_phone,
                    manage_type,
                    manage_email,
                    manage_status,
                    manage_cityid,
                    manage_city,
                    manage_cityalt2id,
                    manage_cityalt2,
                    manage_cityalt1id,
                    manage_cityalt1,
                    manage_countryid,
                    manage_country,
                    manage_countrycode,
                    manage_continent,
                    manage_latitude,
                    manage_longitude,
                    manage_geohash,
                    manage_added_date,
                    manage_userid,
                    manage_stylea
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add manages_by_id: line 179'});
                });
            },
            function (result, done) {
                var newUserbyEmailManageName = 'INSERT INTO users_by_email_and_typeactname(email, type, actname, tokenact) VALUES (?, ?, ?, ?)';
                client.execute(newUserbyEmailManageName,[
                    userEmail,
                    'manage',
                    manage_name,
                    tokenmanage
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add users_by_email_and_managename: line 225'});
                });
            },
            function (result, done) {
                const manageIdStr = manage_id.toString();
                const manageIdTable = [
                    manageIdStr
                ];
                var queryManageIdToUserId = 'UPDATE users_by_id SET activities_id=activities_id + ? WHERE user_id=?';
                client.execute(queryManageIdToUserId,[manageIdTable, manage_userid], { prepare: true })
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add activity manage_id in user_by_id'});
                })
            }
        ], function(result){
            if(result){

                //Elastic Search create manage
                var dateBymilisecond = Math.trunc(manage_added_date / 1000);
                var idUserElastic = manage_userid.toString();
                var idManageElastic = manage_id.toString();

                clientElastic.create({
                    id: idManageElastic,
                    index: elasticIndex,
                    body: {
                        activity: 'manage',
                        userid: idUserElastic,
                        name: manage_name,
                        type: manage_type,
                        style: manage_style,
                        status: manage_status,
                        city: manage_city,
                        region: manage_cityalt1,
                        dept: manage_cityalt2,
                        country: manage_country,
                        countrycode: manage_countrycode,
                        continent: manage_continent,
                        location: {
                            lat: manage_latitude,
                            lon: manage_longitude
                        },
                        addedDate: dateBymilisecond
                    }
                }, (err, result) => {
                    if (err) {
                        return res.status(500).json({'error': err});
                    } else {
                        var tokenWhitoutBear = token.replace('Bearer ', '');
                        var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
                        client.execute(getActivityToken, [userEmail], function (err, resultactivity){ // only for update activity here (not in front)
                            if (err){
                                return res.status(201).json({
                                    'token': tokenWhitoutBear,
                                    'activity': 'unable to find activity'
                                });
                            } else {
                                if(resultactivity.rowLength >= 1){
                                    // boucle pour créer la sortie json... 
                                    var activity = {};
                                    var hasband = false;
                                    var hassite = false;
                                    var hasmanage = false;
                                    var hasorga = false;
                                    var hasservice = false;
                                    var hasequip = false;
                                    for ( var i = 0, len = resultactivity.rowLength; i < len; ++i ) {
                                        var type = resultactivity.rows[i].type;
                                        var actname = resultactivity.rows[i].actname;
                                        var tokenact = resultactivity.rows[i].tokenact;
                                        var keyToken = type + '_' + actname;
                                        var tokenTarget = {};
                                        tokenTarget[keyToken] = tokenact;
                                        activity = Object.assign(activity, tokenTarget);
                                        if (type === 'band') {
                                            hasband = true;
                                        }
                                        if (type === 'site') {
                                            hassite = true;
                                        }
                                        if (type === 'manage') {
                                            hasmanage = true;
                                        }
                                        if (type === 'orga') {
                                            hasorga = true;
                                        }
                                        if (type === 'service') {
                                            hasservice = true;
                                        }
                                        if (type === 'equip') {
                                            hasequip = true;
                                        }
                                    }
                                    return res.status(201).json({
                                        'token': tokenWhitoutBear,
                                        activity,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                    
                                } else {
                                    return res.status(201).json({
                                        'token': token,
                                        'activity': null,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                }
                            }
                        });
                        // return res.status(201).json({
                        //     'token' : token,
                        //     'activity': tokenband
                        // });
                    }
                })
                
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    registerorga: function (req, res) {
        // TODO: To implement
 
        

        // Params
        var token = req.headers['authorization'];
        var orga_id = cassandra.types.TimeUuid.now();
        var orga_name = req.body.nameorga;
        var orga_adress = req.body.adressorga;
        var orga_phone = req.body.phoneorga;
        var orga_email = req.body.emailorga;
        var orga_type = req.body.typeorga;
        var orga_status = req.body.statusorga;
        var orga_cityid = req.body.cityid;
        var orga_city = req.body.city;
        var orga_cityalt2id = req.body.cityalt2id;
        var orga_cityalt2 = req.body.cityalt2;
        var orga_cityalt1id = req.body.cityalt1id;
        var orga_cityalt1 = req.body.cityalt1;
        var orga_countryid = req.body.countryid;
        var orga_country = req.body.country;
        var orga_countrycode = req.body.countrycode;
        var orga_continent = req.body.continent;
        var orga_latitude = req.body.latOrga;
        var orga_longitude = req.body.lngOrga;
        var orga_stylea = req.body.styleorga;
        var orga_style = [orga_stylea];
        
        var orga_added_date = cassandra.types.generateTimestamp();
        // var str1 = 'Bearer';
        // var token = str1.concat(' ', req.body.token);


        var latitude = eval(orga_latitude);
        var longitude = eval(orga_longitude);
        

        var orga_geohash = geohash.encode(latitude, longitude);

        var userSatus = jwtUtils.getUserId(token);
        const orga_userid = userSatus[0];
        var tokenValidDate = userSatus[1];
        var userEmail = userSatus[2];

        var tokenorga = jwtUtils.generateTokenFororga({
            orga_id: orga_id,
            orga_name: orga_name,
            orga_type: orga_type,
            orga_email: orga_email,
            orga_status: orga_status
        });
        

        if (orga_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        if(orga_name === null || orga_type === null || orga_cityid === null || orga_status === null || orga_email === null || orga_phone === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        

        if(orga_name.lenght >= 13 || orga_name.lenght <= 3){
            return res.status(400).json({'error': 'wrong organame (must be lenght 4 - 12)'});
        }

        

        // if(!band_email){
        //     var band_email = null;
        // } else {
        //     if(!EMAIL_REGEX.test(band_email)){
        //         return res.status(400).json({'error': 'email is not valid'});
        //     }
        // }

        // if(!EMAIL_REGEX.test(band_email)){
        //     return res.status(400).json({'error': 'email is not valid'});
        // }

        

        asyncLib.waterfall([
            function(done){
                var newOrgaById = 'INSERT INTO orgas_by_id(orga_id, orga_name, orga_adress, orga_phone, orga_type, orga_email, orga_status, orga_cityid, orga_city, orga_cityalt2id, orga_cityalt2, orga_cityalt1id, orga_cityalt1, orga_countryid, orga_country, orga_countrycode, orga_continent, orga_latitude, orga_longitude, orga_geohash, orga_added_date, orga_userid, orga_styles) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                client.execute(newOrgaById,[
                    orga_id,
                    orga_name,
                    orga_adress,
                    orga_phone,
                    orga_type,
                    orga_email,
                    orga_status,
                    orga_cityid,
                    orga_city,
                    orga_cityalt2id,
                    orga_cityalt2,
                    orga_cityalt1id,
                    orga_cityalt1,
                    orga_countryid,
                    orga_country,
                    orga_countrycode,
                    orga_continent,
                    orga_latitude,
                    orga_longitude,
                    orga_geohash,
                    orga_added_date,
                    orga_userid,
                    orga_stylea
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add orgas_by_id: line 179'});
                });
            },
            function (result, done) {
                var newUserbyEmailOrgaName = 'INSERT INTO users_by_email_and_typeactname(email, type, actname, tokenact) VALUES (?, ?, ?, ?)';
                client.execute(newUserbyEmailOrgaName,[
                    userEmail,
                    'orga',
                    orga_name,
                    tokenorga
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add users_by_email_and_organame: line 225'});
                });
            },
            function (result, done) {
                const orgaIdStr = orga_id.toString();
                const orgaIdTable = [
                    orgaIdStr
                ];
                var queryOrgaIdToUserId = 'UPDATE users_by_id SET activities_id=activities_id + ? WHERE user_id=?';
                client.execute(queryOrgaIdToUserId,[orgaIdTable, orga_userid], { prepare: true })
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add activity orga_id in user_by_id'});
                })
            }
        ], function(result){
            if(result){

                //Elastic Search create orga
                var dateBymilisecond = Math.trunc(orga_added_date / 1000);
                var idUserElastic = orga_userid.toString();
                var idOrgaElastic = orga_id.toString();

                clientElastic.create({
                    id: idOrgaElastic,
                    index: elasticIndex,
                    body: {
                        activity: 'orga',
                        userid: idUserElastic,
                        name: orga_name,
                        type: orga_type,
                        style: orga_style,
                        status: orga_status,
                        city: orga_city,
                        region: orga_cityalt1,
                        dept: orga_cityalt2,
                        country: orga_country,
                        countrycode: orga_countrycode,
                        continent: orga_continent,
                        location: {
                            lat: orga_latitude,
                            lon: orga_longitude
                        },
                        addedDate: dateBymilisecond
                    }
                }, (err, result) => {
                    if (err) {
                        return res.status(500).json({'error': err});
                    } else {
                        var tokenWhitoutBear = token.replace('Bearer ', '');
                        var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
                        client.execute(getActivityToken, [userEmail], function (err, resultactivity){ // only for update activity here (not in front)
                            if (err){
                                return res.status(201).json({
                                    'token': tokenWhitoutBear,
                                    'activity': 'unable to find activity'
                                });
                            } else {
                                if(resultactivity.rowLength >= 1){
                                    // boucle pour créer la sortie json... 
                                    var activity = {};
                                    var hasband = false;
                                    var hassite = false;
                                    var hasmanage = false;
                                    var hasorga = false;
                                    var hasservice = false;
                                    var hasequip = false;
                                    for ( var i = 0, len = resultactivity.rowLength; i < len; ++i ) {
                                        var type = resultactivity.rows[i].type;
                                        var actname = resultactivity.rows[i].actname;
                                        var tokenact = resultactivity.rows[i].tokenact;
                                        var keyToken = type + '_' + actname;
                                        var tokenTarget = {};
                                        tokenTarget[keyToken] = tokenact;
                                        activity = Object.assign(activity, tokenTarget);
                                        if (type === 'band') {
                                            hasband = true;
                                        }
                                        if (type === 'site') {
                                            hassite = true;
                                        }
                                        if (type === 'manage') {
                                            hasmanage = true;
                                        }
                                        if (type === 'orga') {
                                            hasorga = true;
                                        }
                                        if (type === 'service') {
                                            hasservice = true;
                                        }
                                        if (type === 'equip') {
                                            hasequip = true;
                                        }
                                    }
                                    return res.status(201).json({
                                        'token': tokenWhitoutBear,
                                        activity,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                    
                                } else {
                                    return res.status(201).json({
                                        'token': token,
                                        'activity': null,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                }
                            }
                        });
                        // return res.status(201).json({
                        //     'token' : token,
                        //     'activity': tokenband
                        // });
                    }
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    registerservices: function (req, res) {
        // TODO: To implement
 
        

        // Params
        var token = req.headers['authorization'];
        var services_id = cassandra.types.TimeUuid.now();
        var services_name = req.body.nameservices;
        var services_adress = req.body.adressservices;
        var services_phone = req.body.phoneservices;
        var services_email = req.body.emailservices;
        var services_services = req.body.servicesservices;
        var services_status = req.body.statusservices;
        var services_cityid = req.body.cityid;
        var services_city = req.body.city;
        var services_cityalt2id = req.body.cityalt2id;
        var services_cityalt2 = req.body.cityalt2;
        var services_cityalt1id = req.body.cityalt1id;
        var services_cityalt1 = req.body.cityalt1;
        var services_countryid = req.body.countryid;
        var services_country = req.body.country;
        var services_countrycode = req.body.countrycode;
        var services_continent = req.body.continent;
        var services_latitude = req.body.latServices;
        var services_longitude = req.body.lngServices;
        var services_stylea = req.body.styleservices;
        var services_style = [services_stylea];
        
        var services_added_date = cassandra.types.generateTimestamp();
        // var str1 = 'Bearer';
        // var token = str1.concat(' ', req.body.token);

        


        var latitude = eval(services_latitude);
        var longitude = eval(services_longitude);
        

        var services_geohash = geohash.encode(latitude, longitude);

        var userSatus = jwtUtils.getUserId(token);
        const services_userid = userSatus[0];
        var tokenValidDate = userSatus[1];
        var userEmail = userSatus[2];

        

        var tokenservices = jwtUtils.generateTokenForservices({
            services_id: services_id,
            services_name: services_name,
            services_services: services_services,
            services_email: services_email,
            services_status: services_status
        });

        
        

        if (services_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        if(services_name === null || services_services === null || services_cityid === null || services_status === null || services_email === null || services_phone === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        

        if(services_name.lenght >= 13 || services_name.lenght <= 3){
            return res.status(400).json({'error': 'wrong servicesname (must be lenght 4 - 12)'});
        }

        

        // if(!band_email){
        //     var band_email = null;
        // } else {
        //     if(!EMAIL_REGEX.test(band_email)){
        //         return res.status(400).json({'error': 'email is not valid'});
        //     }
        // }

        // if(!EMAIL_REGEX.test(band_email)){
        //     return res.status(400).json({'error': 'email is not valid'});
        // }

        

        asyncLib.waterfall([
            function(done){
                var newServicesById = 'INSERT INTO services_by_id(service_id, service_name, service_adress, service_phone, service_services, service_email, service_status, service_cityid, service_city, service_cityalt2id, service_cityalt2, service_cityalt1id, service_cityalt1, service_countryid, service_country, service_countrycode, service_continent, service_latitude, service_longitude, service_geohash, service_added_date, service_userid, service_styles) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                client.execute(newServicesById,[
                    services_id,
                    services_name,
                    services_adress,
                    services_phone,
                    services_services,
                    services_email,
                    services_status,
                    services_cityid,
                    services_city,
                    services_cityalt2id,
                    services_cityalt2,
                    services_cityalt1id,
                    services_cityalt1,
                    services_countryid,
                    services_country,
                    services_countrycode,
                    services_continent,
                    services_latitude,
                    services_longitude,
                    services_geohash,
                    services_added_date,
                    services_userid,
                    services_stylea
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add services_by_id: line 179'});
                });
            },
            function (result, done) {
                var newUserbyEmailServicesName = 'INSERT INTO users_by_email_and_typeactname(email, type, actname, tokenact) VALUES (?, ?, ?, ?)';
                client.execute(newUserbyEmailServicesName,[
                    userEmail,
                    'service',
                    services_name,
                    tokenservices
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add users_by_email_and_servicename: line 225'});
                });
            },
            function (result, done) {
                const servicesIdStr = services_id.toString();
                const servicesIdTable = [
                    servicesIdStr
                ];
                var queryServiceIdToUserId = 'UPDATE users_by_id SET activities_id=activities_id + ? WHERE user_id=?';
                client.execute(queryServiceIdToUserId,[servicesIdTable, services_userid], { prepare: true })
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add activity service_id in user_by_id'});
                })
            }
        ], function(result){
            if(result){

                //Elastic Search create service
                var dateBymilisecond = Math.trunc(services_added_date / 1000);
                var idUserElastic = services_userid.toString();
                var idServiceElastic = services_id.toString();

                clientElastic.create({
                    id: idServiceElastic,
                    index: elasticIndex,
                    body: {
                        activity: 'service',
                        userid: idUserElastic,
                        name: services_name,
                        type: 'service',
                        style: services_style,
                        service: services_services,
                        status: services_status,
                        city: services_city,
                        region: services_cityalt1,
                        dept: services_cityalt2,
                        country: services_country,
                        countrycode: services_countrycode,
                        continent: services_continent,
                        location: {
                            lat: services_latitude,
                            lon: services_longitude
                        },
                        addedDate: dateBymilisecond
                    }
                }, (err, result) => {
                    if (err) {
                        return res.status(500).json({'error': err});
                    } else {
                        var tokenWhitoutBear = token.replace('Bearer ', '');
                        var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
                        client.execute(getActivityToken, [userEmail], function (err, resultactivity){ // only for update activity here (not in front)
                            if (err){
                                return res.status(201).json({
                                    'token': tokenWhitoutBear,
                                    'activity': 'unable to find activity'
                                });
                            } else {
                                if(resultactivity.rowLength >= 1){
                                    // boucle pour créer la sortie json... 
                                    var activity = {};
                                    var hasband = false;
                                    var hassite = false;
                                    var hasmanage = false;
                                    var hasorga = false;
                                    var hasservice = false;
                                    var hasequip = false;
                                    for ( var i = 0, len = resultactivity.rowLength; i < len; ++i ) {
                                        var type = resultactivity.rows[i].type;
                                        var actname = resultactivity.rows[i].actname;
                                        var tokenact = resultactivity.rows[i].tokenact;
                                        var keyToken = type + '_' + actname;
                                        var tokenTarget = {};
                                        tokenTarget[keyToken] = tokenact;
                                        activity = Object.assign(activity, tokenTarget);
                                        if (type === 'band') {
                                            hasband = true;
                                        }
                                        if (type === 'site') {
                                            hassite = true;
                                        }
                                        if (type === 'manage') {
                                            hasmanage = true;
                                        }
                                        if (type === 'orga') {
                                            hasorga = true;
                                        }
                                        if (type === 'service') {
                                            hasservice = true;
                                        }
                                        if (type === 'equip') {
                                            hasequip = true;
                                        }
                                    }
                                    return res.status(201).json({
                                        'token': tokenWhitoutBear,
                                        activity,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                    
                                } else {
                                    return res.status(201).json({
                                        'token': token,
                                        'activity': null,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                }
                            }
                        });
                        // return res.status(201).json({
                        //     'token' : token,
                        //     'activity': tokenband
                        // });
                    }
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    registerequip: function (req, res) {
        // TODO: To implement
 
        

        // Params
        var token = req.headers['authorization'];
        var equip_id = cassandra.types.TimeUuid.now();
        var equip_name = req.body.nameequip;
        var equip_adress = req.body.adressequip;
        var equip_phone = req.body.phoneequip;
        var equip_email = req.body.emailequip;
        var equip_type = req.body.typeequip;
        var equip_cityid = req.body.cityid;
        var equip_city = req.body.city;
        var equip_cityalt2id = req.body.cityalt2id;
        var equip_cityalt2 = req.body.cityalt2;
        var equip_cityalt1id = req.body.cityalt1id;
        var equip_cityalt1 = req.body.cityalt1;
        var equip_countryid = req.body.countryid;
        var equip_country = req.body.country;
        var equip_countrycode = req.body.countrycode;
        var equip_continent = req.body.continent;
        var equip_latitude = req.body.latEquip;
        var equip_longitude = req.body.lngEquip;
        
        var equip_added_date = cassandra.types.generateTimestamp();
        // var str1 = 'Bearer';
        // var token = str1.concat(' ', req.body.token);


        var latitude = eval(equip_latitude);
        var longitude = eval(equip_longitude);
        

        var equip_geohash = geohash.encode(latitude, longitude);

        var userSatus = jwtUtils.getUserId(token);
        const equip_userid = userSatus[0];
        var tokenValidDate = userSatus[1];
        var userEmail = userSatus[2];

        var tokenequip = jwtUtils.generateTokenForequip({
            equip_id: equip_id,
            equip_name: equip_name,
            equip_type: equip_type,
            equip_email: equip_email
        });
        

        if (equip_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        if(equip_name === null || equip_type === null || equip_cityid === null || equip_email === null || equip_phone === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        

        if(equip_name.lenght >= 13 || equip_name.lenght <= 3){
            return res.status(400).json({'error': 'wrong equipname (must be lenght 4 - 12)'});
        }

        

        // if(!band_email){
        //     var band_email = null;
        // } else {
        //     if(!EMAIL_REGEX.test(band_email)){
        //         return res.status(400).json({'error': 'email is not valid'});
        //     }
        // }

        // if(!EMAIL_REGEX.test(band_email)){
        //     return res.status(400).json({'error': 'email is not valid'});
        // }

        

        asyncLib.waterfall([
            function(done){
                var newEquipById = 'INSERT INTO equips_by_id(equip_id, equip_name, equip_adress, equip_phone, equip_type, equip_email, equip_cityid, equip_city, equip_cityalt2id, equip_cityalt2, equip_cityalt1id, equip_cityalt1, equip_countryid, equip_country, equip_countrycode, equip_continent, equip_latitude, equip_longitude, equip_geohash, equip_added_date, equip_userid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
                client.execute(newEquipById,[
                    equip_id,
                    equip_name,
                    equip_adress,
                    equip_phone,
                    equip_type,
                    equip_email,
                    equip_cityid,
                    equip_city,
                    equip_cityalt2id,
                    equip_cityalt2,
                    equip_cityalt1id,
                    equip_cityalt1,
                    equip_countryid,
                    equip_country,
                    equip_countrycode,
                    equip_continent,
                    equip_latitude,
                    equip_longitude,
                    equip_geohash,
                    equip_added_date,
                    equip_userid
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add equips_by_id: line 179'});
                });
            },
            function (result, done) {
                var newUserbyEmailEquipName = 'INSERT INTO users_by_email_and_typeactname(email, type, actname, tokenact) VALUES (?, ?, ?, ?)';
                client.execute(newUserbyEmailEquipName,[
                    userEmail,
                    'equip',
                    equip_name,
                    tokenequip
                ])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add users_by_email_and_equipname: line 225'});
                });
            },
            function (result, done) {
                const equipIdStr = equip_id.toString();
                const equipIdTable = [
                    equipIdStr
                ];
                var queryEquipIdToUserId = 'UPDATE users_by_id SET activities_id=activities_id + ? WHERE user_id=?';
                client.execute(queryEquipIdToUserId,[equipIdTable, equip_userid], { prepare: true })
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add activity service_id in user_by_id'});
                })
            }
        ], function(result){
            if(result){

                //Elastic Search create equip
                var dateBymilisecond = Math.trunc(equip_added_date / 1000);
                var idUserElastic = equip_userid.toString();
                var idEquipElastic = equip_id.toString();

                clientElastic.create({
                    id: idEquipElastic,
                    index: elasticIndex,
                    body: {
                        activity: 'equip',
                        userid: idUserElastic,
                        name: equip_name,
                        type: equip_type,
                        city: equip_city,
                        region: equip_cityalt1,
                        dept: equip_cityalt2,
                        country: equip_country,
                        countrycode: equip_countrycode,
                        continent: equip_continent,
                        location: {
                            lat: equip_latitude,
                            lon: equip_longitude
                        },
                        addedDate: dateBymilisecond
                    }
                }, (err, result) => {
                    if (err) {
                        return res.status(500).json({'error': err});
                    } else {
                        var tokenWhitoutBear = token.replace('Bearer ', '');
                        var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
                        client.execute(getActivityToken, [userEmail], function (err, resultactivity){ // only for update activity here (not in front)
                            if (err){
                                return res.status(201).json({
                                    'token': tokenWhitoutBear,
                                    'activity': 'unable to find activity'
                                });
                            } else {
                                if(resultactivity.rowLength >= 1){
                                    // boucle pour créer la sortie json... 
                                    var activity = {};
                                    var hasband = false;
                                    var hassite = false;
                                    var hasmanage = false;
                                    var hasorga = false;
                                    var hasservice = false;
                                    var hasequip = false;
                                    for ( var i = 0, len = resultactivity.rowLength; i < len; ++i ) {
                                        var type = resultactivity.rows[i].type;
                                        var actname = resultactivity.rows[i].actname;
                                        var tokenact = resultactivity.rows[i].tokenact;
                                        var keyToken = type + '_' + actname;
                                        var tokenTarget = {};
                                        tokenTarget[keyToken] = tokenact;
                                        activity = Object.assign(activity, tokenTarget);
                                        if (type === 'band') {
                                            hasband = true;
                                        }
                                        if (type === 'site') {
                                            hassite = true;
                                        }
                                        if (type === 'manage') {
                                            hasmanage = true;
                                        }
                                        if (type === 'orga') {
                                            hasorga = true;
                                        }
                                        if (type === 'service') {
                                            hasservice = true;
                                        }
                                        if (type === 'equip') {
                                            hasequip = true;
                                        }
                                    }
                                    return res.status(201).json({
                                        'token': tokenWhitoutBear,
                                        activity,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                    
                                } else {
                                    return res.status(201).json({
                                        'token': token,
                                        'activity': null,
                                        hasband,
                                        hassite,
                                        hasmanage,
                                        hasorga,
                                        hasservice,
                                        hasequip
                                    });
                                }
                            }
                        });
                        // return res.status(201).json({
                        //     'token' : token,
                        //     'activity': tokenband
                        // });
                    }
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    registeractivity: function (req, res) {
        var token = req.headers['authorization'];
        var firstname = req.body.firstname;
        var lastname = req.body.lastname;
        var phone = req.body.phone;

        var userSatus = jwtUtils.getUserId(token);
        const activity_userid = userSatus[0];
        var tokenValidDate = userSatus[1];
        var userEmail = userSatus[2];
        var userisValidate = userSatus[3];

        

        if (activity_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        if (userisValidate === false) {
            return res.status(400).json({'error': 'permission refuse'});
        }

        if(firstname === null || lastname === null || phone === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        asyncLib.waterfall([
            function (done) {
                var updateUserByEmail = 'INSERT INTO users_by_email(email, isactivity, firstname, lastname, phone) VALUES (?, ?, ?, ?, ?)';
                client.execute(updateUserByEmail,[userEmail, true, firstname, lastname, phone])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot update users_by_email line 391'});
                });
            },
            function (result, done) {
                if(result){
                    var getUserId = 'SELECT user_id, email, name, isvalidate, isactivity, added_date, firstname, lastname, phone FROM users_by_email WHERE email = ?';
                    client.execute(getUserId, [userEmail], function (err, result){
                        if (err){
                            return res.status(500).json({'error': 'unable to generate token'});
                        } else {
                            var idToken = result.rows[0].user_id;
                            var emailToken = result.rows[0].email;
                            var nameToken = result.rows[0].name;
                            var isValidateToken = result.rows[0].isvalidate;
                            var isActivityToken = result.rows[0].isactivity;
                            var addedDateToken = result.rows[0].added_date.getTime();
                            var firstnameToken = result.rows[0].firstname;
                            var lastnameToken = result.rows[0].lastname;
                            var phoneToken = result.rows[0].phone;

                            var dateBymilisecond = Math.trunc(addedDateToken / 1000);
                            // const event = new Date(dateBymilisecond);
                            // console.log(event.toUTCString());
                            tokenUser = jwtUtils.generateTokenForUser({
                                id: idToken,
                                email: emailToken,
                                name: nameToken,
                                isValidate: isValidateToken,
                                isActivity: isActivityToken,
                                addedDate: dateBymilisecond,
                                firstname: firstnameToken,
                                lastname: lastnameToken,
                                phone: phoneToken
                            });
                            createBucket.bucket(idToken.toString());
                            done(tokenUser);
                        }
                    });
                } else {
                    return res.status(500).json({'error': 'cannot log on user'});
                }
            }
        ], function (tokenUser) {
            var getActivityToken = 'SELECT email, type, actname, tokenact FROM users_by_email_and_typeactname WHERE email = ?';
            client.execute(getActivityToken, [userEmail], function (err, result){
                if (err){
                    return res.status(201).json({
                        'token': tokenUser,
                        'activity': null
                    });
                } else {
                    if(result.rowLength >= 1){
                        // boucle pour créer la sortie json... 
                        var activity = {};
                        var hasband = false;
                        var hassite = false;
                        var hasmanage = false;
                        var hasorga = false;
                        var hasservice = false;
                        var hasequip = false;
                        for ( var i = 0, len = result.rowLength; i < len; ++i ) {
                            var type = result.rows[i].type;
                            var actname = result.rows[i].actname;
                            var tokenact = result.rows[i].tokenact;
                            var keyToken = type + '_' + actname;
                            var tokenTarget = {};
                            tokenTarget[keyToken] = tokenact;
                            activity = Object.assign(activity, tokenTarget);
                            if (type === 'band') {
                                hasband = true;
                            }
                            if (type === 'site') {
                                hassite = true;
                            }
                            if (type === 'manage') {
                                hasmanage = true;
                            }
                            if (type === 'orga') {
                                hasorga = true;
                            }
                            if (type === 'service') {
                                hasservice = true;
                            }
                            if (type === 'equip') {
                                hasequip = true;
                            }
                        }
                        return res.status(201).json({
                            'token': tokenUser,
                            activity,
                            hasband,
                            hassite,
                            hasmanage,
                            hasorga,
                            hasservice,
                            hasequip
                        });
                        
                    } else {
                        return res.status(201).json({
                            'token': tokenUser,
                            'activity': null
                        });
                    }
                }
            });

        });



    },
    readband: function (req, res) {
        var token = req.headers['authorization'];
        var band_id = req.body.bandId;

        var userSatus = jwtUtils.getUserId(token);
        const band_userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (band_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        var getBand = 'SELECT * FROM bands_by_id WHERE band_id = ?';
        client.execute(getBand, [band_id])
            .then(function (result) {
                var dateBymilisecond = Math.trunc(result.rows[0].band_added_date / 1000);
                return res.status(201).json({
                    'bandReadid': result.rows[0].band_id,
                    'bandReaduserid': result.rows[0].band_userid,
                    'bandReadname': result.rows[0].band_name,
                    'bandReadaddedDate': dateBymilisecond,
                    'bandReadcity': result.rows[0].band_city,
                    'bandReadcityalt1': result.rows[0].band_cityalt1,
                    'bandReadcityalt2': result.rows[0].band_cityalt2,
                    'bandReadcountry': result.rows[0].band_country,
                    'bandReadcountrycode': result.rows[0].band_countrycode,
                    'bandReadcontinent': result.rows[0].band_continent,
                    'bandReadlatitude': result.rows[0].band_latitude,
                    'bandReadlongitude': result.rows[0].band_longitude,
                    'bandReademail': result.rows[0].band_email,
                    'bandReadstyle': result.rows[0].style,
                    'bandReadrole': result.rows[0].role
                });
            })
            .catch(function (err) {
                return res.status(201).json({
                    'bandReadid': ''
                })
                
            })   
    },
    readsite: function (req, res) {
        var token = req.headers['authorization'];
        var site_id = req.body.siteId;

        var userSatus = jwtUtils.getUserId(token);
        const site_userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (site_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        var getSite = 'SELECT * FROM sites_by_id WHERE site_id = ?';
        client.execute(getSite, [site_id])
            .then(function (result) {
                var dateBymilisecond = Math.trunc(result.rows[0].site_added_date / 1000);
                return res.status(201).json({
                    'siteReadid': result.rows[0].site_id,
                    'siteReaduserid': result.rows[0].site_userid,
                    'siteReadname': result.rows[0].site_name,
                    'siteReadaddedDate': dateBymilisecond,
                    'siteReadaddress': result.rows[0].site_adress,
                    'siteReadcity': result.rows[0].site_city,
                    'siteReadcityalt1': result.rows[0].site_cityalt1,
                    'siteReadcityalt2': result.rows[0].site_cityalt2,
                    'siteReadcountry': result.rows[0].site_country,
                    'siteReadcountrycode': result.rows[0].site_countrycode,
                    'siteReadcontinent': result.rows[0].site_continent,
                    'siteReadlatitude': result.rows[0].site_latitude,
                    'siteReadlongitude': result.rows[0].site_longitude,
                    'siteReademail': result.rows[0].site_email,
                    'siteReadphone': result.rows[0].site_phone,
                    'siteReadtype': result.rows[0].site_type,
                    'siteReadstyle': result.rows[0].site_styles,
                    'siteReadcapacity': result.rows[0].site_capacity
                });
            })
            .catch(function (err) {
                return res.status(201).json({
                    'siteReadid': ''
                })
                
            })   
    },
    readmanage: function (req, res) {
        var token = req.headers['authorization'];
        var manage_id = req.body.manageId;

        var userSatus = jwtUtils.getUserId(token);
        const manage_userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (manage_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        var getManage = 'SELECT * FROM manages_by_id WHERE manage_id = ?';
        client.execute(getManage, [manage_id])
            .then(function (result) {
                var dateBymilisecond = Math.trunc(result.rows[0].manage_added_date / 1000);
                return res.status(201).json({
                    'manageReadid': result.rows[0].manage_id,
                    'manageReaduserid': result.rows[0].manage_userid,
                    'manageReadname': result.rows[0].manage_name,
                    'manageReadaddedDate': dateBymilisecond,
                    'manageReadaddress': result.rows[0].manage_adress,
                    'manageReadcity': result.rows[0].manage_city,
                    'manageReadcityalt1': result.rows[0].manage_cityalt1,
                    'manageReadcityalt2': result.rows[0].manage_cityalt2,
                    'manageReadcountry': result.rows[0].manage_country,
                    'manageReadcountrycode': result.rows[0].manage_countrycode,
                    'manageReadcontinent': result.rows[0].manage_continent,
                    'manageReadlatitude': result.rows[0].manage_latitude,
                    'manageReadlongitude': result.rows[0].manage_longitude,
                    'manageReademail': result.rows[0].manage_email,
                    'manageReadphone': result.rows[0].manage_phone,
                    'manageReadtype': result.rows[0].manage_type,
                    'manageReadstyle': result.rows[0].manage_styles,
                    'manageReadstatus': result.rows[0].manage_status
                });
            })
            .catch(function (err) {
                return res.status(201).json({
                    'manageReadid': ''
                })
                
            })   
    },
    readorga: function (req, res) {
        var token = req.headers['authorization'];
        var orga_id = req.body.orgaId;

        var userSatus = jwtUtils.getUserId(token);
        const orga_userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (orga_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        var getOrga = 'SELECT * FROM orgas_by_id WHERE orga_id = ?';
        client.execute(getOrga, [orga_id])
            .then(function (result) {
                var dateBymilisecond = Math.trunc(result.rows[0].orga_added_date / 1000);
                return res.status(201).json({
                    'orgaReadid': result.rows[0].orga_id,
                    'orgaReaduserid': result.rows[0].orga_userid,
                    'orgaReadname': result.rows[0].orga_name,
                    'orgaReadaddedDate': dateBymilisecond,
                    'orgaReadaddress': result.rows[0].orga_adress,
                    'orgaReadcity': result.rows[0].orga_city,
                    'orgaReadcityalt1': result.rows[0].orga_cityalt1,
                    'orgaReadcityalt2': result.rows[0].orga_cityalt2,
                    'orgaReadcountry': result.rows[0].orga_country,
                    'orgaReadcountrycode': result.rows[0].orga_countrycode,
                    'orgaReadcontinent': result.rows[0].orga_continent,
                    'orgaReadlatitude': result.rows[0].orga_latitude,
                    'orgaReadlongitude': result.rows[0].orga_longitude,
                    'orgaReademail': result.rows[0].orga_email,
                    'orgaReadphone': result.rows[0].orga_phone,
                    'orgaReadtype': result.rows[0].orga_type,
                    'orgaReadstyle': result.rows[0].orga_styles,
                    'orgaReadstatus': result.rows[0].orga_status
                });
            })
            .catch(function (err) {
                return res.status(201).json({
                    'orgaReadid': ''
                })
                
            })   
    },
    readequip: function (req, res) {
        var token = req.headers['authorization'];
        var equip_id = req.body.equipId;

        var userSatus = jwtUtils.getUserId(token);
        const equip_userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (equip_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        var getEquip = 'SELECT * FROM equips_by_id WHERE equip_id = ?';
        client.execute(getEquip, [equip_id])
            .then(function (result) {
                var dateBymilisecond = Math.trunc(result.rows[0].equip_added_date / 1000);
                return res.status(201).json({
                    'equipReadid': result.rows[0].equip_id,
                    'equipReaduserid': result.rows[0].equip_userid,
                    'equipReadname': result.rows[0].equip_name,
                    'equipReadaddedDate': dateBymilisecond,
                    'equipReadaddress': result.rows[0].equip_adress,
                    'equipReadcity': result.rows[0].equip_city,
                    'equipReadcityalt1': result.rows[0].equip_cityalt1,
                    'equipReadcityalt2': result.rows[0].equip_cityalt2,
                    'equipReadcountry': result.rows[0].equip_country,
                    'equipReadcountrycode': result.rows[0].equip_countrycode,
                    'equipReadcontinent': result.rows[0].equip_continent,
                    'equipReadlatitude': result.rows[0].equip_latitude,
                    'equipReadlongitude': result.rows[0].equip_longitude,
                    'equipReademail': result.rows[0].equip_email,
                    'equipReadphone': result.rows[0].equip_phone,
                    'equipReadtype': result.rows[0].equip_type
                });
            })
            .catch(function (err) {
                return res.status(201).json({
                    'equipReadid': ''
                })
                
            })   
    },
    readservice: function (req, res) {
        var token = req.headers['authorization'];
        var service_id = req.body.serviceId;

        var userSatus = jwtUtils.getUserId(token);
        const service_userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (service_userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        var getService = 'SELECT * FROM services_by_id WHERE service_id = ?';
        client.execute(getService, [service_id])
            .then(function (result) {
                var dateBymilisecond = Math.trunc(result.rows[0].service_added_date / 1000);
                return res.status(201).json({
                    'serviceReadid': result.rows[0].service_id,
                    'serviceReaduserid': result.rows[0].service_userid,
                    'serviceReadname': result.rows[0].service_name,
                    'serviceReadaddedDate': dateBymilisecond,
                    'serviceReadaddress': result.rows[0].service_adress,
                    'serviceReadcity': result.rows[0].service_city,
                    'serviceReadcityalt1': result.rows[0].service_cityalt1,
                    'serviceReadcityalt2': result.rows[0].service_cityalt2,
                    'serviceReadcountry': result.rows[0].service_country,
                    'serviceReadcountrycode': result.rows[0].service_countrycode,
                    'serviceReadcontinent': result.rows[0].service_continent,
                    'serviceReadlatitude': result.rows[0].service_latitude,
                    'serviceReadlongitude': result.rows[0].service_longitude,
                    'serviceReademail': result.rows[0].service_email,
                    'serviceReadphone': result.rows[0].service_phone,
                    'serviceReadstyle': result.rows[0].service_styles,
                    'serviceReadstatus': result.rows[0].service_status,
                    'serviceReadservice': result.rows[0].service_services
                });
            })
            .catch(function (err) {
                return res.status(201).json({
                    'serviceReadid': ''
                })
                
            })   
    }
}