// env file
require('dotenv').config();
// Imports
jwtVerifyToken = require('../utils/verifyToken.utils');


const JWT_SIGN_SECRET = process.env.JWT_SIGN_SECRET_GITLAB; // <=== MUST BE env variable

// Exported functions
module.exports = {
    verifyToken: function (req, res) {

        var token = req.body.token;

        var userSatus = jwtVerifyToken.getId(token);
        return res.status(200).json({'result': userSatus});
        
    }
}