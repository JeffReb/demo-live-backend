// env file
require('dotenv').config();
// Imports
var express = require('express');


var bodyParser = require('body-parser');
var cors = require('cors');
var apiRouter = require('./apiRouter').router;

var whitelist = [
    'http://localhost:4200',
    'http://127.0.0.1:4200',
    'feature.live-assault.com',
    'clem.live-assault.com',
    'clemfeature.live-assault.com',
    'jeff.live-assault.com',
    'jefffeature.live-assault.com',
    'staging.live-assault.com',
    'develop.live-assault.com',
    'www.feature.live-assault.com',
    'www.clem.live-assault.com',
    'www.clemfeature.live-assault.com',
    'www.jeff.live-assault.com',
    'www.jefffeature.live-assault.com',
    'www.staging.live-assault.com',
    'www.develop.live-assault.com',
    'beta.live-assault.com',
    'gradus-beta.live-assault.com',
    'www.beta.live-assault.com',
    'www.gradus-beta.live-assault.com',
    'live-assault.com',
    'gradus.live-assault.com',
    'www.live-assault.com',
    'www.gradus.live-assault.com',
    'https://feature.live-assault.com',
    'https://clem.live-assault.com',
    'https://clemfeature.live-assault.com',
    'https://jeff.live-assault.com',
    'https://jefffeature.live-assault.com',
    'https://staging.live-assault.com',
    'https://develop.live-assault.com',
    'https://www.feature.live-assault.com',
    'https://www.clem.live-assault.com',
    'https://www.clemfeature.live-assault.com',
    'https://www.jeff.live-assault.com',
    'https://www.jefffeature.live-assault.com',
    'https://www.staging.live-assault.com',
    'https://www.develop.live-assault.com',
    'https://beta.live-assault.com',
    'https://gradus-beta.live-assault.com',
    'https://www.beta.live-assault.com',
    'https://www.gradus-beta.live-assault.com',
    'https://live-assault.com',
    'https://gradus.live-assault.com',
    'https://www.live-assault.com',
    'https://www.gradus.live-assault.com',
    /\.live-assault\.com$/,
    /\.mytoobox\.net$/
]


var corsOptions = {
    origin: function (origin, callback) {
      if (whitelist.indexOf(origin) !== -1 || !origin) {
        callback(null, true)
      } else {
        callback(new Error('Not allowed by CORS'))
      }
    },
    credentials: true
  }
  



// Instantiate server
var server = express();
var http = require('http').createServer(server);
var io = require('socket.io')(http);

server.use(cors(corsOptions)); // allow access-control-allow-origin



// env variables
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;
const SENDMAIL_PASSWORD = process.env.SENDMAIL_PASSWORD;
const SENDMAIL_ADRESS = process.env.SENDMAIL_ADRESS;
const URLTOKENVALID = process.env.URLTOKENVALID;




// Body Parser configuration
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());


// Configure routes
server.get('/', function(req, res){
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send(`<h1>Bonjour sur mon serveur</h1>`);
});

// assign apiRouter
server.use('/api/', apiRouter);

// Launch server
// server.listen(5000, function(){
//     console.log('Server en écoute :)');
// });
http.listen(5000, function () {
    console.log('socket server :)');
});

//socketIo ecoute

io.on('connection', function (socket) {
    // console.log('a user is connected');
    socket.on('disconnect', function () {
        // console.log('a user is disconnected');
    })
    socket.on('chat message', function (msg) {
        // console.log('message recu: ' + msg[0].idUserDest);
        io.emit(msg[0].idUserDest, msg);
    })
    socket.on('contact request', function (msg) {
        // console.log('message recu: ' + msg[0].idUserDest);
        io.emit(msg[0].idUserDest, msg);
    })
    socket.on('contact accept', function (msg) {
      // console.log('message recu: ' + msg[0].idUserDest);
      io.emit(msg[0].idUserDest, msg);
  })
});