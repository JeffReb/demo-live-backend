// env file
require('dotenv').config();
// Imports
var jwtUtils = require('../../jwt.utils');

var cassandra = require('cassandra-driver');
var asyncLib = require('async');

// env variables
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;


var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);


var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});

client.connect(function (err, result) {
    console.log('index: cassandra connected to assetsService');
});

// Routes
module.exports = {
    putLogoService: function (req, res) {
        // console.log('filename', req.file.filename);
        var token = req.headers['authorization'];
        var idService = req.headers['curractid'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var asset_id = cassandra.types.TimeUuid.now();
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        // console.log('idService: ', idService);

        asyncLib.waterfall([
            function (done) {
                var putLogoServiceDefault = 'INSERT INTO assets_by_serviceid (service_id, asset_type, filename) VALUES (?, ?, ?)';
                client.execute(putLogoServiceDefault,[idService, 'logoservice', req.file.filename])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);
                    return res.status(500).json({'error': 'cannot add asset logoservicedefault in database'});
                });
            },
            function (result, done) {
                var putLogoServiceList = 'INSERT INTO asset_by_serviceid_and_assetid (service_id, asset_type, asset_id, filename) VALUES (?, ?, ?, ?)';
                client.execute(putLogoServiceList,[idService, 'logoservice', asset_id, req.file.filename])
                .then(function(result){
                    done(result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'cannot add asset logoservicelist in database'});
                });
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    id: req.file.filename
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    getLogoService: function (req, res) {
        // Params
        var token = req.headers['authorization'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var serviceidBody = req.body.serviceId;
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        var getLogoServiceExist = 'SELECT filename FROM assets_by_serviceid WHERE service_id = ? AND asset_type = ?';
        client.execute(getLogoServiceExist, [serviceidBody, 'logoservice'])
        .then(function(result){
            if (result.rowLength === 1){
                var Filename = result.rows[0].filename;
                return res.status(201).json({
                    'filename': Filename
                });
            } else {
                return res.status(201).json({
                    'filename': 'logoservice not found'
                });
            }
        })
        .catch(function(err) {
            return res.status(500).json({'error': 'unable to verify if LogoService Exist'});
        });
    }
}



