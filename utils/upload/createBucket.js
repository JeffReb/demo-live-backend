var Minio = require('minio');

const minioEndPoint = process.env.MINIO_ENDPOINT;
const minioAcessKey = process.env.MINIO_ACCESS_KEY;
const minioSecretKey = process.env.MINIO_SECRET_KEY;

var minioClient = new Minio.Client({
    endPoint: minioEndPoint,
    port: 9000,
    useSSL: false,
    accessKey: minioAcessKey,
    secretKey: minioSecretKey
});

exports.bucket = (userid) => {
    // console.log(userid);
    minioClient.bucketExists(userid, function(err, exists) {
        if (exists) {
            // console.log('bucket exist');
            minioClient.getBucketPolicy(userid, function(err, policy) {
                if (err) throw err
              
                // console.log(`Bucket policy file: ${policy}`)
              })
            return true;
        } else {
            var bucket = userid;
            var policy = `{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":["*"]},"Action":["s3:GetBucketLocation","s3:ListBucket"],"Resource":["arn:aws:s3:::` + bucket + `"]},{"Effect":"Allow","Principal":{"AWS":["*"]},"Action":["s3:GetObject"],"Resource":["arn:aws:s3:::` + bucket + `/*"]}]}`
            // console.log('bucket not exist');
                minioClient.makeBucket(userid, 'us-east-1', function(err) {
                    if (err) {
                        // console.log('Error creating bucket.', err);
                        return false;
                    }
                    // console.log('Bucket created successfully in "us-east-1".');
                    return true;
                  });
                  minioClient.setBucketPolicy(userid, policy, function(err) {
                    if (err) throw err
                  
                    // console.log('Bucket policy set')
                  });
            return false;
        }
        });
}