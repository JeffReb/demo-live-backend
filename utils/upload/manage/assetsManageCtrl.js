// env file
require('dotenv').config();
// Imports
var jwtUtils = require('../../jwt.utils');

var cassandra = require('cassandra-driver');
var asyncLib = require('async');

// env variables
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;


var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);


var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});

client.connect(function (err, result) {
    console.log('index: cassandra connected to assetsManage');
});

// Routes
module.exports = {
    putLogoManage: function (req, res) {
        // console.log('filename', req.file.filename);
        var token = req.headers['authorization'];
        var idManage = req.headers['curractid'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var asset_id = cassandra.types.TimeUuid.now();
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        // console.log('idManage: ', idManage);

        asyncLib.waterfall([
            function (done) {
                var putLogoManageDefault = 'INSERT INTO assets_by_manageid (manage_id, asset_type, filename) VALUES (?, ?, ?)';
                client.execute(putLogoManageDefault,[idManage, 'logomanage', req.file.filename])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);
                    return res.status(500).json({'error': 'cannot add asset logomanagedefault in database'});
                });
            },
            function (result, done) {
                var putLogoManageList = 'INSERT INTO asset_by_manageid_and_assetid (manage_id, asset_type, asset_id, filename) VALUES (?, ?, ?, ?)';
                client.execute(putLogoManageList,[idManage, 'logomanage', asset_id, req.file.filename])
                .then(function(result){
                    done(result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'cannot add asset logomanagelist in database'});
                });
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    id: req.file.filename
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    getLogoManage: function (req, res) {
        // Params
        var token = req.headers['authorization'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var manageidBody = req.body.manageId;
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        var getLogoManageExist = 'SELECT filename FROM assets_by_manageid WHERE manage_id = ? AND asset_type = ?';
        client.execute(getLogoManageExist, [manageidBody, 'logomanage'])
        .then(function(result){
            if (result.rowLength === 1){
                var Filename = result.rows[0].filename;
                return res.status(201).json({
                    'filename': Filename
                });
            } else {
                return res.status(201).json({
                    'filename': 'logomanage not found'
                });
            }
        })
        .catch(function(err) {
            return res.status(500).json({'error': 'unable to verify if LogoManage Exist'});
        });
    }
}



