var Minio = require('minio');
var fs = require('fs');
var jwtUtils = require('../../jwt.utils');
const path = require('path');
var mime = require('mime-types');

const directory = './tmp';



const minioEndPoint = process.env.MINIO_ENDPOINT;
const minioAcessKey = process.env.MINIO_ACCESS_KEY;
const minioSecretKey = process.env.MINIO_SECRET_KEY;

var minioClient = new Minio.Client({
    endPoint: minioEndPoint,
    port: 9000,
    useSSL: false,
    accessKey: minioAcessKey,
    secretKey: minioSecretKey
});

exports.save = (req, res, next) => {
    var token = req.headers['authorization'];
    var userSatus = jwtUtils.getUserId(token);
    const userid = userSatus[0];
    if (userid < 0){
        return res.status(401).json({'error': 'wrong token'});
    }

    let ext = mime.extension(req.file.mimetype);

    if (ext != 'gif' && ext != 'png' && ext != 'jpeg') {
        return res.status(401).json({'error': 'wrong token'});
    }

    
    var metaData = {
        'Content-Type': req.file.mimetype,
        'X-Amz-Meta-Testing': 1234,
        'example': 5678
    }
    minioClient.fPutObject(userid, req.file.filename, req.file.path, metaData, function(error, etag) {
      if(error) {
          return console.log(error);
      }
      fs.readdir(directory, (err, files) => {
        if (err) throw err;
      
        for (const file of files) {
            if ( file === '.gitkeep') {
                // console.log(file);
            } else {
                fs.unlink(path.join(directory, file), err => {
                    if (err) throw err;
                  });
            }
        }
      });
      next();
    });
    
}