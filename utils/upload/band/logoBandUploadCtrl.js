// env file
require('dotenv').config();


// Imports
var jwtUtils = require('../../jwt.utils');
var Multer = require('multer');
var path = require('path');
// var storage = Multer.memoryStorage();
var storage = Multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './tmp')
    },
    filename: function (req, file, cb) {
      let extName = path.extname(file.originalname);
      cb(null, file.fieldname + '-' + Date.now() + extName)
    }
  })
var upload = Multer({storage: storage});


// Routes

module.exports = (req, res, next) => {

    var token = req.headers['authorization'];
    var userSatus = jwtUtils.getUserId(token);
    const userid = userSatus[0];
    if (userid < 0){
      return res.status(401).json({'error': 'wrong token'});
    }
  
    upload.single('logoband')(req, res, function (err) {
      //Catching and handling errors of multer
      if (err) {
        return console.log(error) 
      } else {
        // console.log(req.body.test);
        // res.send(req.file);
        // putImg(req, res);
      }
      //Everything is ok
      
      next();
    })
  }
