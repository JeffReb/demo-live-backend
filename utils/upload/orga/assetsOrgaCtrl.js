// env file
require('dotenv').config();
// Imports
var jwtUtils = require('../../jwt.utils');

var cassandra = require('cassandra-driver');
var asyncLib = require('async');

// env variables
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;


var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);


var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});

client.connect(function (err, result) {
    console.log('index: cassandra connected to assetsOrga');
});

// Routes
module.exports = {
    putLogoOrga: function (req, res) {
        // console.log('filename', req.file.filename);
        var token = req.headers['authorization'];
        var idOrga = req.headers['curractid'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var asset_id = cassandra.types.TimeUuid.now();
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        // console.log('idOrga: ', idOrga);

        asyncLib.waterfall([
            function (done) {
                var putLogoOrgaDefault = 'INSERT INTO assets_by_orgaid (orga_id, asset_type, filename) VALUES (?, ?, ?)';
                client.execute(putLogoOrgaDefault,[idOrga, 'logoorga', req.file.filename])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);
                    return res.status(500).json({'error': 'cannot add asset logoorgadefault in database'});
                });
            },
            function (result, done) {
                var putLogoOrgaList = 'INSERT INTO asset_by_orgaid_and_assetid (orga_id, asset_type, asset_id, filename) VALUES (?, ?, ?, ?)';
                client.execute(putLogoOrgaList,[idOrga, 'logoorga', asset_id, req.file.filename])
                .then(function(result){
                    done(result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'cannot add asset logoorgalist in database'});
                });
            }
        ], function (result) {
            if(result){
                return res.status(201).json({
                    id: req.file.filename
                })
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
        
    },
    getLogoOrga: function (req, res) {
        // Params
        var token = req.headers['authorization'];
        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var orgaidBody = req.body.orgaId;
        
        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        }

        var getLogoOrgaExist = 'SELECT filename FROM assets_by_orgaid WHERE orga_id = ? AND asset_type = ?';
        client.execute(getLogoOrgaExist, [orgaidBody, 'logoorga'])
        .then(function(result){
            if (result.rowLength === 1){
                var Filename = result.rows[0].filename;
                return res.status(201).json({
                    'filename': Filename
                });
            } else {
                return res.status(201).json({
                    'filename': 'logoorga not found'
                });
            }
        })
        .catch(function(err) {
            return res.status(500).json({'error': 'unable to verify if LogoOrga Exist'});
        });
    }
}



