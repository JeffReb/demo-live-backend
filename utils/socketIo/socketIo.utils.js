// env file
require('dotenv').config();

// imports
var cassandra = require('cassandra-driver');
var jwtUtils = require('../jwt.utils');
var asyncLib = require('async');

// env variables cassandra
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;

var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);

var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});


client.connect(function (err, result) {
    console.log('index: cassandra connected to socketIo.utils');
});

// Exported functions
module.exports = {
    setConnectedStatus: function (req, res) {
        var token = req.headers['authorization'];

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        const connectedStatusValue = false;

        var connectedStatus = req.body.connected;
        if (connectedStatus === 'true') {
            this.connectedStatusValue = true;
        } else {
            this.connectedStatusValue = false;
        }

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var readOnlineSet = 'SELECT online_set FROM users_by_id WHERE user_id = ?';
                client.execute(readOnlineSet, [userid])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot read user online setting'});
                })  
            },
            function (result, done) {
                var resultOnlineSet = result.rows[0].online_set;
                // console.log('onlineSet: ', resultOnlineSet);
                if (resultOnlineSet === null) {
                    var setOnlineSet = 'UPDATE users_by_id SET online_set=? WHERE user_id=?';
                    client.execute(setOnlineSet, [true, userid])
                    .then(function (result) {
                        done(null, result);
                    })
                    .catch(function (err) {
                        console.log(err);
                        return res.status(500).json({'error': 'unable to set onlineset to true when is null'});
                    })
                } else {
                    // console.log('online is note null');
                    done(null, result);
                }
            },
            function (result, done) {
                //console.log('connectedStatusValue: ', this.connectedStatusValue);
                var setConnectedStatusValue = this.connectedStatusValue;
                var setConnectValue = 'UPDATE users_by_id SET connected=? WHERE user_id=?';
                client.execute(setConnectValue, [setConnectedStatusValue, userid])
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'unable to set connected value'});
                })
            }
        ], function (result) {
            if (result) {
                return res.status(201).json({'onlineSet': result});
            } else {
                return res.status(201).json({'onlineSet': 'no result'});
            }
        })
    }
}