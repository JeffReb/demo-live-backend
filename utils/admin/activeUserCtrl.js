// env file
require('dotenv').config();
// Imports
var someObject = require('./prepa400-total-new02.json');

var someObjectMail = require('./testEmail.json');

var cassandra = require('cassandra-driver');
var asyncLib = require('async');

// env variables cassandra
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;




var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);

var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace, 
    authProvider: authProvider,
    encoding: { 
        map: Map,
        set: Set
      }
});

client.connect(function (err, result) {
    console.log('index: cassandra connected');
});


module.exports = {
    confValidate: function (req, res) {

        

        var count = Object.keys(someObject).length;
        const arrayDataMail = Object.keys(someObject).map(key => someObject[key]);
        
        console.log('someObjectCount: ', count);
        // console.log('arrayJsonObj: ', arrayJsonObj);

        for (let i = 0; i < count; i++) {
            const emailprefix = arrayDataMail[i]['useremail'];
            const email = emailprefix + '@yahoo.fr'
            
            

            console.log(email);
            var updateUserByEmail = 'INSERT INTO users_by_email(email, isvalidate) VALUES (?, ?)';
                client.execute(updateUserByEmail,[email, true])
                .then(function(result){
                    
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot update users_by_email line 391'});
                });
        }

        return res.status(201).json({'ok': 'users are validated !!'});
        
    },
    updateSetType: function (req, res) {

        

        var count = Object.keys(someObjectMail).length;
        const arrayDataMail = Object.keys(someObjectMail).map(key => someObjectMail[key]);
        
        console.log('someObjectCount: ', count);
        // console.log('arrayJsonObj: ', arrayJsonObj);

        for (let i = 0; i < count; i++) {
            const emailprefix = arrayDataMail[i]['useremail'];
            const email = emailprefix + '@yahoo.fr'
            const namesite = arrayDataMail[i]['namesite'];
            const style = arrayDataMail[i]['style'];

            // const notetype = [ // <= for remove test !!!
            //     style
            // ]

            const notetype = [ // <= for insert or update test !!! be carefull that the values in json are differents (the list type must bee unique)
                email,
                style
            ]
            
            

            console.log(email);
            // var updateUserByEmail = 'UPDATE testusers_by_email SET notetype=? WHERE email=?'; // update send new value and replace old
            // var updateUserByEmail = 'UPDATE testusers_by_email SET notetype=notetype - ? WHERE email=?'; // remove one element from list respect order vaules
            var updateUserByEmail = 'INSERT INTO testusers_by_email(email, notetype) VALUES (?, ?)'; // insert new values in set type column and keep old !!!
                client.execute(updateUserByEmail,[email, notetype], { prepare: true }) // <= inverse values notetype, email when update
                .then(function(result){
                    
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot update users_by_email line 391'});
                });
        }

        return res.status(201).json({'ok': 'users are validated !!'});
        
    },
    updateMapType: function (req, res) {

        

        var count = Object.keys(someObjectMail).length;
        const arrayDataMail = Object.keys(someObjectMail).map(key => someObjectMail[key]);
        
        console.log('someObjectCount: ', count);
        // console.log('arrayJsonObj: ', arrayJsonObj);

        for (let i = 0; i < count; i++) {
            const emailprefix = arrayDataMail[i]['useremail'];
            const email = emailprefix + '@yahoo.fr'
            const namesite = arrayDataMail[i]['namesite'];
            const style = arrayDataMail[i]['style'];

            // const notetype = [ // <= for remove test !!!
            //     style
            // ]
            

            const notemap = {
                idUser: emailprefix,
                idRoom: namesite,
                checkedState: 'true'
            }

            
            
            

            console.log(email);
            // var updateUserByEmail = 'UPDATE testusers_by_email SET notemap=? WHERE email=?'; // update send new value and replace old
            // var updateUserByEmail = 'UPDATE testusers_by_email SET notemap=notemap - ? WHERE email=?'; // remove one element from list respect order vaules
            var updateUserByEmail = 'INSERT INTO testusers_by_email(email, notemap) VALUES (?, ?)'; // insert new values in set type column and keep old !!!
                client.execute(updateUserByEmail,[email, notemap], { prepare: true }) // <= inverse values notetype, email when update
                .then(function(result){
                    
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot update users_by_email line 391'});
                });
        }

        return res.status(201).json({'ok': 'users are validated !!'});
        
    },
    updateUserActivitiesSet: function (req, res) {

        asyncLib.waterfall([
            function (done) {
                var getBandId = 'SELECT site_id, site_userid FROM sites_by_id LIMIT 3000';
                client.execute(getBandId)
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    return res.status(500).json({
                        'error': 'unable to get user from user_id'
                    })
                })
            },
            function (result, done) {
                console.log('resultBandRowLength: ', result.rowLength);
                var resutBandlength = result.rowLength;
                var queryBandIdToUserId = 'UPDATE users_by_id SET activities_id=activities_id + ? WHERE user_id=?';
                var params = [];
                var queries = [];
                var objQuery = {};
                if (resutBandlength > 0) {
                    for ( var i = 0, len = resutBandlength; i < len; ++i ){
                        var bandId = [result.rows[i].site_id.toString()];
                        // console.log('resultBandid: ', bandId);
                        var userId = result.rows[i].site_userid.toString();
                        // console.log('resultBandUserid: ', userId);
                        objQuery = {
                            query: queryBandIdToUserId,
                            params: [bandId, userId]
                        };
                        queries.push(objQuery);
                    }
                    client.batch(queries, { prepare: true })
                    .then(function (result) {
                        console.log('result batch queries: ', result);
                        done(result);
                    })
                    .catch(function (err) {
                        return res.status(500).json({'error': err});
                    })
                } else {
                    done(result);
                }
                
            }
        ], function (result) {
            if (result){
                return res.status(201).json({
                    'messages': 'Ok !!'
                });
            } else {
                return res.status(500).json({'error': 'cannot read message'});
            }
        });
        
    }
}