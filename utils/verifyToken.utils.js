// Imports
var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = process.env.JWT_SIGN_SECRET_GITLAB; // <=== MUST BE env variable

module.exports = {
    getId: function (token) {
        var Id = -1;
        if(token != null) {
            try {
                var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (jwtToken != null)
                    Id = jwtToken.bandId;
            } catch (err) { }
        }
        return [Id];
    }
}