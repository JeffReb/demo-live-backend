var nodemailer = require('nodemailer');


const SENDMAIL_PASSWORD = process.env.SENDMAIL_PASSWORD;
const SENDMAIL_ADRESS = process.env.SENDMAIL_ADRESS;
const URLTOKENVALID = process.env.URLTOKENVALID;

var EmailUtil = {
  sendEmail : function sendEmail(to, userName, newToken, callback) {
      var options = {
          host: 'smtp.sfr.fr',
          port: 465,
          secure: true,
          auth: {
              user: SENDMAIL_ADRESS,
              pass: SENDMAIL_PASSWORD
          }
      };
      var transport = nodemailer.createTransport(options);

      var htmlBody = `<h1>Welcome  ${userName}</h1><p>Thank you for your subscription in live-assault to confirm your registration</p><p>Please click on this link:</p><p> <a href="https://${URLTOKENVALID}/emailloginconf/${newToken}">https://${URLTOKENVALID}/emailloginconf/${newToken}</a></p>`

      transport.sendMail({
          from: SENDMAIL_ADRESS, 
          to: to,
          subject: 'Email account confirmation from live-assault',
          html: htmlBody
      }, function(err, info) {
          var val;
          if (err) {
              console.log(err);
              val = 'err';
          } else {
              // console.log('Email sent: ' + info.response);
              val = 'ok'; 
          }
          callback({Status: 'OK', ID: val});
      });
  }
};

module.exports = EmailUtil

// Exported functions
// module.exports = {
//     sendTokenForEmailConf: function(userData) {
//         console.log('token: ', userData);
//         var transporter = nodemailer.createTransport({
//             host: 'smtp.sfr.fr',
//             port: 465,
//             secure: true,
//             auth: {
//               user: SENDMAIL_ADRESS,
//               pass: SENDMAIL_PASSWORD
//             }
//           });
          
//           var mailOptions = {
//             from: SENDMAIL_ADRESS,
//             to: 'jeanfrancois.rebaud@gmail.com',
//             subject: 'Sending Email using Node.js',
//             text: 'That was easy!'
//           };
          
//           var EmailUtil = transporter.sendMail(mailOptions, function(error, info){
//             var result;
//             if (error) {
//               console.log(error);
//               result = false;
//             } else {
//               console.log('Email sent: ' + info.response);
//               result = true;
//             }
//           }); 
//     }
// }