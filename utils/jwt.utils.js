// Imports
var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = process.env.JWT_SIGN_SECRET_GITLAB; // <=== MUST BE env variable

// Exported functions
module.exports = {
    generateTokenForUser: function(userData) {
        return jwt.sign({
            userId: userData.id,
            userEmail: userData.email,
            userName: userData.name,
            isValidate: userData.isValidate,
            isActivity: userData.isActivity,
            addedDate: userData.addedDate,
            firstname: userData.firstname,
            lastname: userData.lastname,
            phone: userData.phone
        },
        JWT_SIGN_SECRET,
        {
            expiresIn: '96h'
        })
    },
    parseAuthorization: function (authorization) {
        return (authorization != null) ? authorization.replace('Bearer ', '') : null;
    },
    getUserId: function (authorization) {
        var userId = -1;
        var tokenValidDate = false;
        var userEmail = null;
        var userisValidate = false;
        var token = module.exports.parseAuthorization(authorization);
        if(token != null) {
            try {
                var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (jwtToken != null)
                    userId = jwtToken.userId;
                    dateCreate = jwtToken.iat;
                    userEmail = jwtToken.userEmail;
                    userisValidate = jwtToken.isValidate;
                    if (dateCreate < 1580698026){ // 1573452997 date of update the code to restart connexion users see: https://www.epochconverter.com/
                        tokenValidDate = false;
                    } else {
                        tokenValidDate = true;
                    }
            } catch (err) { }
        }
        return [userId, tokenValidDate, userEmail, userisValidate];
    },
    generateTokenForEmail: function(userData) {
        return jwt.sign({
            userEmail: userData.email,
            isValidate: userData.isValidate
            // userEmail: userData.email,
            // userName: userData.name,
            // isValidate: userData.isValidate,
            // isActivity: userData.isActivity
        },
        JWT_SIGN_SECRET,
        {
            expiresIn: '1h'
        })
    },
    getUserConfirmId: function (token) {
        var userId = -1;
        if(token != null) {
            try {
                var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (jwtToken != null)
                    userId = jwtToken.userId;
                    userEmail = jwtToken.userEmail;
                    userName = jwtToken.userName;
                    isValidate = jwtToken.isValidate;
                    isActivity = jwtToken.isActivity;
            } catch (err) { }
        }
        return [userId, userEmail, userName, isValidate, isActivity];
    },
    getTokenEmailAccept: function (token) {
        var userId = -1;
        if(token != null) {
            try {
                var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (jwtToken != null)
                    userId = jwtToken.userId;
                    isValidate = jwtToken.isValidate;
            } catch (err) { }
        }
        return [userId, isValidate];
    },
    getUserIdent: function (token) {
        var userId = -1;
        if(token != null) {
            try {
                var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (jwtToken != null)
                    userId = jwtToken.userId;
                    userEmail = jwtToken.userEmail;
                    userName = jwtToken.userName;
            } catch (err) { }
        }
        return [userId, userEmail, userName];
    },
    getTokenEmailLoginConf: function (token) {
        var isValidate = false;
        var userEmail = '';
        if (token != null){
            try {
                var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
                if (jwtToken != null)
                    userEmail = jwtToken.userEmail;
                    isValidate = jwtToken.isValidate;
            } catch (err) {}
        }
        return [userEmail, isValidate];
        
    },
    generateTokenForBand: function(bandData) {
        return jwt.sign({
            bandId: bandData.band_id,
            bandName: bandData.band_name,
            bandStyle: bandData.style,
            bandEmail: bandData.band_email,
            bandRole: bandData.band_role,
            type: 'band'
        },
        JWT_SIGN_SECRET)
    },
    generateTokenForsite: function(siteData) {
        return jwt.sign({
            siteId: siteData.site_id,
            siteName: siteData.site_name,
            siteType: siteData.site_type,
            siteEmail: siteData.site_email,
            siteCapacity: siteData.site_capacity,
            type: 'site'
        },
        JWT_SIGN_SECRET)
    },
    generateTokenFormanage: function(manageData) {
        return jwt.sign({
            manageId: manageData.manage_id,
            manageName: manageData.manage_name,
            manageType: manageData.manage_type,
            manageEmail: manageData.manage_email,
            manageStatus: manageData.manage_status,
            type: 'manage'
        },
        JWT_SIGN_SECRET)
    },
    generateTokenFororga: function(orgaData) {
        return jwt.sign({
            orgaId: orgaData.orga_id,
            orgaName: orgaData.orga_name,
            orgaType: orgaData.orga_type,
            orgaEmail: orgaData.orga_email,
            orgaStatus: orgaData.orga_status,
            type: 'orga'
        },
        JWT_SIGN_SECRET)
    },
    generateTokenForservices: function(servicesData) {
        return jwt.sign({
            servicesId: servicesData.services_id,
            servicesName: servicesData.services_name,
            servicesServices: servicesData.services_services,
            servicesEmail: servicesData.services_email,
            servicesStatus: servicesData.services_status,
            type: 'service'
        },
        JWT_SIGN_SECRET)
    },
    generateTokenForequip: function(equipData) {
        return jwt.sign({
            equipId: equipData.equip_id,
            equipName: equipData.equip_name,
            equipType: equipData.equip_type,
            equipEmail: equipData.equip_email,
            type: 'equip'
        },
        JWT_SIGN_SECRET)
    }
}