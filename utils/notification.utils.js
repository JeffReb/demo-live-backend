// env file
require('dotenv').config();

// imports
var cassandra = require('cassandra-driver');
var jwtUtils = require('./jwt.utils');
var asyncLib = require('async');
// var express = require('express');
// var server = express();
// var http = require('http').createServer(server);
// var io = require('socket.io')(http);

// env variables cassandra
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;

var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);

var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});

const resultat = 'test';

client.connect(function (err, result) {
    console.log('index: cassandra connected to notifications');
});

// Exported functions
module.exports = {
    sendNotificationContact: function (toUserId, fromUser, noteType) {
        var added_date = cassandra.types.generateTimestamp();

        var sendNote = 'INSERT INTO cnot_byuserid_and_fromuser(user_id, fromuser_id, added_date, note_type, note_checked) VALUES (?, ?, ?, ?, ?)';
        client.execute(sendNote,[toUserId, fromUser, added_date, noteType, false])
        .then(function (result) {
            const resultat = 'true';
            return resultat;
        })
        .catch(function (err) {
            console.log('line 42', err);
            const resultat = 'false';
            return resultat;
        });

        return resultat;
        
    },
    checkNotificationContact: function (req, res) {

        var token = req.headers['authorization'];

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        var fromUserId = req.body.fromUserId;

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        
        // console.log('userid: ', userid);
        // console.log('noteId: ', noteId);

        var checkNote = 'UPDATE cnot_byuserid_and_fromuser SET note_checked=? WHERE user_id=? AND fromuser_id=?';
        client.execute(checkNote,[true, userid, fromUserId])
        .then(function (result) {
            return res.status(201).json({'notification': result});
        })
        .catch(function (err) {
            console.log('line 137', err);
            
            return res.status(201).json({'notification': err});
        });

        
    },
    readNotificationContact: function (req, res) {
        var token = req.headers['authorization'];

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var readNote = 'SELECT * FROM cnot_byuserid_and_fromuser WHERE user_id = ? LIMIT 20';
                client.execute(readNote,[userid])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot read user in notification'});
                })
            },
            function (result, done) {
                if(result.rowLength === 0) {
                    var resultat = [];
                    return res.status(201).json({
                        'notificationContact': resultat
                    });
                } else {
                    done(result);
                }
            }
        ], function (result) {
            if (result){
                var resultat = [];
                for ( var i = 0, len = result.rowLength; i < len; ++i ) {
                    const addedDate = result.rows[i].added_date;
                    const dateBymilisecond = Math.trunc(addedDate / 1000);
                    resultat.push({
                        fromUser: result.rows[i].fromuser_id,
                        noteDate: dateBymilisecond,
                        noteCheck : result.rows[i].note_checked,
                        noteType : result.rows[i].note_type
                    })
                }
                return res.status(201).json({'notificationContact': resultat});
            } else {
                return res.status(500).json({'error': 'cannot read user in notification'});
            }
        });

        
        
    },
    sendNotificationMessage: function (toUserId, fromUser) {
        var added_date = cassandra.types.generateTimestamp();

        asyncLib.waterfall([
            function (done) {
                var isChatWithMe = 'SELECT chat_with FROM users_by_id WHERE user_id = ?'; // who is chat in this moment with my contact?
                client.execute(isChatWithMe, [toUserId])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    const resultat = 'false';
                    return resultat;
                })
            },
            function (result, done) { // if my contact chat with me inline actualy ?
                var ChatWithId = result.rows[0].chat_with;
                if (ChatWithId === null) { // I never chat with this contact or he's not connected
                    // console.log('it s NOT me !!! line 166');
                    done(null, result)
                } else {
                    var ChatWithIdStr = ChatWithId.toString();
                    // console.log('ChatWithId: ', ChatWithId);
                    // console.log('fromUser: ', fromUser);
                    if (ChatWithIdStr === fromUser) { // I'm chating actualy with him inline
                        // console.log('it s me !!!');
                        const resultat = 'false';
                        return resultat;
                    } else {
                        // console.log('it s NOT me !!! line 177'); // he certainly chat actually with another person or he's not connected
                        done(null, result)
                    }
                }
            },
            function (result, done) { // search the last notification I have send to this contact
                var LastNotification = 'SELECT note_checked FROM messnot_byuserid_and_fromusertimst WHERE user_id = ? AND fromuser_id = ? LIMIT 1';
                client.execute(LastNotification, [toUserId, fromUser])
                .then(function (result) {
                    // console.log('search last notification');
                    // console.log('result.rowLength', result.rowLength);
                    done(null, result);
                })
                .catch(function (err) {
                    console.log('cannot select note_checked from last notification', err);
                    const resultat = 'false';
                    return resultat;
                })
            },
            function (result, done) {
                if (result.rowLength === 0) { // I never send notification to this contact
                    // console.log('I never send notification to this contact so continue...');
                    done(null, result);
                } else {
                    if (result.rows[0].note_checked === true) { // this contact check my last notification
                        // console.log('This contact check my last notification so continue...');
                        done(null, result);
                    } else {
                        // console.log('this contact have no checked my last notification so break and finish');
                        const resultat = 'false';
                        return resultat; // return false to not send new notification (afin de pas polluer les notifs)
                    }
                }
            },
            function (result, done) {
                var sendNewNotification = 'INSERT INTO messnot_byuserid_and_fromusertimst(user_id, fromuser_id, added_date, note_type, note_checked) VALUES (?, ?, ?, ?, ?)';
                client.execute(sendNewNotification, [toUserId, fromUser, added_date, 'message', false])
                .then(function (result) {
                    // console.log('Notification insertion !!');
                    done(result);
                })
                .catch(function (err) {
                    console.log('Cannot insert notification error: ', err);
                    const resultat = 'false';
                    return resultat; // impossible to send notification
                })
            }
        ], function (result) {
            if (result) {
                // console.log('continue test');
                // const newMess = 'You receive a new message';
                // const messSocket = [
                // {
                //     idUserDest: toUserId,
                //     content: {
                //     idFrom: fromUser,
                //     noteType: 'messagenot',
                //     mess: newMess
                //     }
                // }
                // ];
                
                // io.on('connection', function (socket) {
                //     io.emit(messSocket[0].idUserDest, messSocket[0]);
                // });
                
                const resultat = 'true';
                return resultat;
            } else {
                const resultat = 'false';
                return resultat;
            }
        });
    },
    readNotificationMessage: function (req, res) {
        var token = req.headers['authorization'];

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        asyncLib.waterfall([
            function (done) {
                var readNote = 'SELECT * FROM messnot_byuserid_and_fromusertimst WHERE user_id = ? LIMIT 20';
                client.execute(readNote,[userid])
                .then(function (result) {
                    done(null, result);
                })
                .catch(function (err) {
                    console.log(err);
                    return res.status(500).json({'error': 'cannot read user in notification'});
                })
            },
            function (result, done) {
                if(result.rowLength === 0) {
                    var resultat = [];
                    return res.status(201).json({
                        'notificationMessage': resultat
                    });
                } else {
                    done(result);
                }
            }
        ], function (result) {
            if (result){
                var resultat = [];
                for ( var i = 0, len = result.rowLength; i < len; ++i ) {
                    const addedDate = result.rows[i].added_date;
                    const dateBymilisecond = Math.trunc(addedDate / 1000);
                    resultat.push({
                        fromUser: result.rows[i].fromuser_id,
                        noteDate: dateBymilisecond,
                        noteCheck : result.rows[i].note_checked,
                        noteType : result.rows[i].note_type
                    })
                }
                return res.status(201).json({'notificationMessage': resultat});
            } else {
                return res.status(500).json({'error': 'cannot read user in notification'});
            }
        });

        
        
    },
    checkNotificationMessage: function (req, res) {

        var token = req.headers['authorization'];

        var userSatus = jwtUtils.getUserId(token);
        const userid = userSatus[0];
        var tokenValidDate = userSatus[1];

        var fromUserId = req.body.fromUserId;

        if (userid < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            if (!tokenValidDate){
                return res.status(401).json({'error': 'we restart token due to an application update'});
            }
        }

        
        asyncLib.waterfall([
            function (done) {
                var readAddedDate = 'SELECT added_date FROM messnot_byuserid_and_fromusertimst WHERE user_id = ? AND fromuser_id = ? LIMIT 1';
                client.execute(readAddedDate,[userid, fromUserId])
                .then(function (result) {
                    done(result);
                })
                .catch(function (err) {
                    console.log('cannot read added_date from last notification', err);
                    return res.status(201).json({'notification': err});
                })
            }
        ], function (result) {
            var addedDate = result.rows[0].added_date;
            var checkNote = 'UPDATE messnot_byuserid_and_fromusertimst SET note_checked = ? WHERE user_id = ? AND fromuser_id = ? AND added_date = ?';
            client.execute(checkNote,[true, userid, fromUserId, addedDate])
            .then(function (result) {
                return res.status(201).json({'notification': result});
            })
            .catch(function (err) {
                 console.log('line 137', err);
                return res.status(201).json({'notification': err});
            })
        });
        
    }
}